<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});;


Auth::routes();
	
Route::get('/home', 'HomeController@index')->name('home');
 

Route::group(['prefix'=>'waiter'], function(){
	Route::get('/', ['as' =>'waiter.index', 'uses'=>'WaiterController@index']);
	Route::get('/show', ['as' =>'waiter.show', 'uses'=>'WaiterController@show']);
	Route::get('/detail/{id}', ['as' =>'waiter.detail.show', 'uses'=>'WaiterController@showdetail']);
	Route::post('/update/{id}', ['as' =>'waiter.store', 'uses'=>'WaiterController@update']);
	Route::group(['prefix' => 'report'], function(){
		Route::get('/', ['as' =>'report.index', 'uses'=>'WaiterController@indexReport']);
		Route::get('/excel', ['as' =>'report.excel', 'uses'=>'WaiterController@excel']);
		Route::get('/exceltanggal/{t1}/{t2}', ['as' =>'report.exceltanggal', 'uses'=>'WaiterController@exceltanggal']);
		Route::get('/pdf', ['as' =>'report.pdf', 'uses'=>'WaiterController@pdf']);
		Route::get('/pdftanggal/{t1}/{t2}', ['as' =>'report.pdftanggal', 'uses'=>'WaiterController@pdftanggal']);
		Route::post('/tanggal', ['as' =>'report.tanggal', 'uses'=>'WaiterController@tanggal']);
	});
});

Route::group(['prefix' => 'admin'], function(){
	Route::get('/', ['as' =>'admin.index', 'uses'=>'AdminController@index']);
	
	Route::group(['prefix' => 'menu'], function(){
		Route::get('/', ['as' =>'menu.index', 'uses'=>'MenuController@index']);
		Route::get('/create', ['as' =>'menu.create', 'uses'=>'MenuController@create']);
		Route::post('/store', ['as' =>'menu.store', 'uses'=>'MenuController@store']);
		Route::get('/edit/{id}', ['as' =>'menu.edit', 'uses'=>'MenuController@edit']);
		Route::post('/update/{id}', ['as' =>'menu.update', 'uses'=>'MenuController@update']);
		Route::get('/destroy/{id}', ['as' =>'menu.destroy', 'uses'=>'MenuController@destroy']);
	});
	Route::group(['prefix' => 'order'], function(){
		Route::get('/', ['as' =>'order.index', 'uses'=>'OrderController@index']);
		Route::get('/create', ['as' =>'order.create', 'uses'=>'OrderController@create']);
		Route::post('/store', ['as' =>'order.store', 'uses'=>'OrderController@store']);
		Route::post('/fixstore', ['as' =>'order.fixstore', 'uses'=>'OrderController@fixstore']);
		Route::get('/destroy/{id}', ['as' =>'order.destroy', 'uses'=>'OrderController@destroy']);
	});
	Route::group(['prefix' => 'level'], function(){
		Route::get('/', ['as' =>'level.index', 'uses'=>'LevelController@index']);
		Route::get('/create', ['as' =>'level.create', 'uses'=>'LevelController@create']);
		Route::post('/store', ['as' =>'level.store', 'uses'=>'LevelController@store']);
		Route::get('/edit/{id}', ['as' =>'level.edit', 'uses'=>'LevelController@edit']);
		Route::patch('/update/{id}', ['as' =>'level.update', 'uses'=>'LevelController@update']);
		Route::get('/destroy/{id}', ['as' =>'level.destroy', 'uses'=>'LevelController@destroy']);
	});

	Route::group(['prefix' => 'kategori'], function(){
		Route::get('/create', ['as' =>'kategori.create', 'uses'=>'KategoriController@create']);
		Route::post('/store', ['as' =>'kategori.store', 'uses'=>'KategoriController@store']);
		Route::get('/edit/{id}', ['as' =>'kategori.edit', 'uses'=>'KategoriController@edit']);
		Route::post('/update/{id}', ['as' =>'kategori.update', 'uses'=>'KategoriController@update']);
		Route::get('/destroy/{id}', ['as' =>'kategori.destroy', 'uses'=>'KategoriController@destroy']);
		Route::get('/export',['as' => 'kategori.export', 'uses' => 'KategoriController@export']);
		Route::post('/import',['as' => 'kategori.import', 'uses' => 'KategoriController@import']);
	});

	Route::group(['prefix' => 'report'], function(){
		Route::get('/', ['as' =>'report.index', 'uses'=>'ReportController@index']);
		Route::get('/excel', ['as' =>'report.excel', 'uses'=>'ReportController@excel']);
		Route::get('/exceltanggal/{t1}/{t2}', ['as' =>'report.exceltanggal', 'uses'=>'ReportController@exceltanggal']);
		Route::get('/pdf', ['as' =>'report.pdf', 'uses'=>'ReportController@pdf']);
		Route::get('/pdftanggal/{t1}/{t2}', ['as' =>'report.pdftanggal', 'uses'=>'ReportController@pdftanggal']);
		Route::post('/tanggal', ['as' =>'report.tanggal', 'uses'=>'ReportController@tanggal']);
	});

	Route::group(['prefix' => 'transaksi'], function(){
		Route::get('/', ['as' =>'transaksi.index', 'uses'=>'TransaksiController@index']);
		Route::get('/show/{id}', ['as' =>'transaksi.show', 'uses'=>'TransaksiController@show']);
		Route::post('/store', ['as' =>'transaksi.store', 'uses'=>'TransaksiController@store']);
	});
});


Route::group(['prefix'=>'pelanggan'], function(){
	Route::get('/', ['as' => 'pelanggan.index', 'uses' => 'PelangganController@index']);
});

Route::group(['prefix' => 'koki'],function(){
	Route::get('/', ['as'=>'koki.index', 'uses'=>'KokiController@index']);
	Route::get('/show/{id}', ['as'=>'koki.show', 'uses'=>'KokiController@show']);
	// Route::get('/edit/{id}/', ['as'=>'koki.show', 'uses'=>'KokiController@edit']);
	Route::post('/update/{id}/', ['as'=>'koki.update', 'uses'=>'KokiController@update']);
});
Route::group(['prefix'=>'kasir'], function(){
	Route::get('/', ['as' => 'kasir.index', 'uses'=>'KasirController@index']);
	Route::get('/show/{id}', ['as' =>'kasir.show', 'uses'=>'KasirController@show']);
	Route::post('/store', ['as' =>'kasir.store', 'uses'=>'KasirController@store']);
	Route::group(['prefix' => 'report'], function(){
		Route::get('/', ['as' =>'KasirReport.index', 'uses'=>'KasirReportController@index']);
		Route::get('/excel', ['as' =>'KasirReport.excel', 'uses'=>'KasirReportController@excel']);
		Route::get('/exceltanggal/{t1}/{t2}', ['as' =>'KasirReport.exceltanggal', 'uses'=>'KasirReportController@exceltanggal']);
		Route::get('/pdf', ['as' =>'KasirReport.pdf', 'uses'=>'KasirReportController@pdf']);
		Route::get('/pdftanggal/{t1}/{t2}', ['as' =>'KasirReport.pdftanggal', 'uses'=>'KasirReportController@pdftanggal']);
		Route::post('/tanggal', ['as' =>'KasirReport.tanggal', 'uses'=>'KasirReportController@tanggal']);
	});
});
Route::group(['prefix'=>'owner'], function(){
		Route::get('/', ['as' =>'report.index', 'uses'=>'OwnerController@index']);
		Route::get('/excel', ['as' =>'report.excel', 'uses'=>'OwnerController@excel']);
		Route::get('/exceltanggal/{t1}/{t2}', ['as' =>'report.exceltanggal', 'uses'=>'OwnerController@exceltanggal']);
		Route::get('/pdf', ['as' =>'report.pdf', 'uses'=>'OwnerController@pdf']);
		Route::get('/pdftanggal/{t1}/{t2}', ['as' =>'report.pdftanggal', 'uses'=>'OwnerController@pdftanggal']);
		Route::post('/tanggal', ['as' =>'report.tanggal', 'uses'=>'OwnerController@tanggal']);
});