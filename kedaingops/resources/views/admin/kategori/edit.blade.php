@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                    <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                    <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
                    <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
                    <a href="{{ url('admin/report') }}" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
                    <a href="{{ route('transaksi.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Transaksi</span></a>
            </li>
    </li>
@endsection

@section('body')
    <div class="card">
        <div class="card-header">
            <h4>Tambah Kategori</h4>
        </div>
        <div class="card-body">
            <a href="{{ route('kategori.create') }}" class="btn btn-danger float-right">Back</a>
            <div class="row">
                <div class="col-md-5">
                    <form action="{{ url('admin/kategori/update/'.$edit->id) }}" method="post">
                        @csrf
                            <div class="form-group">
                                <label for="">Nama Kategori</label>
                            <input type="text" value="{{ $edit->kategori }}" name="kategori" class="form-control">
                            </div>
                            <button class="btn btn-success">Update</button>    
                    </form>
                </div>
                <div class="col-md-7">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Kategori</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($kategori as $val)
                                    <tr>
                                    <td>{{$loop->index+1 }}</td>
                                    <td>{{ $val->kategori }}</td>
                                    <td>
                                        <a href="{{ url('admin/kategori/edit/'.$val->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                    </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    </div>
@endsection