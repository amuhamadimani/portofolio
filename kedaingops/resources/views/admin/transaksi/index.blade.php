@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                
                    <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                    <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
                    <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
                    <a href="{{ url('admin/report') }}" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
                    <a href="{{ route('transaksi.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Transaksi</span></a>
            </li>
    </li>
@endsection

@section('body')
   <div class="card">
       <div class="card-header">
           <h4>Transaksi</h4>
       </div>
       <div class="card-body">
           <table class="table table-bordered" id="dabel">
               <thead>
                   <tr>
                       <th>No</th>
                       <th>Id Order</th>
                       <th>No Meja</th>
                       <th>Nama Pemesan</th>
                       <th>Action</th>
                   </tr>
               </thead>
               <tbody>
                   @foreach($pesanan as $val)
                        <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $val->id_order }}</td>
                        <td>{{ $val->no_meja }}</td>
                        <td>{{ $val->keterangan }}</td>
                            <td>
                            <a href="{{ url('admin/transaksi/show/'.$val->id_order) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                   @endforeach
               </tbody>
           </table>
       </div>
   </div>
@endsection