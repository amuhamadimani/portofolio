@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                    <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                    <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
                    <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
                    <a href="{{ url('admin/report') }}" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
                    <a href="{{ route('transaksi.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Transaksi</span></a>
            </li>
    </li>
@endsection

@section('body')
    <div class="card">
        <div class="card-header">
            {{ $menu->links() }}
        </div>
    </div>
    <div align="center">
        <button class="btn btn-lg btn-icon btn-warning" data-toggle="modal" data-target="#modalpesanan"><i class="fa fa-shopping-cart"></i></button>
    </div>
    <br>
    <form action="{{ route('order.store') }}" method="post">
        <div class="row">
        @csrf
            <button class="btn btn-info btn-lg" style="position:fixed; right:0; top:200; width:100; z-index:1;">Pesan</button>

    @foreach($menu as $val)
        
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header " style="background-color: whitesmoke;">
                        <h4 class="card-title">{{ $val->nama_menu }}</h4>
                        <input type="hidden" name="id_menu[]" value="{{ $val['id'] }}">

                    </div>
                    <div class="card-body text-center">
                        <img src="{{ asset('assets/img/menu/'.$val->gambar) }}" width="200" height="200" alt="">
                    </div>
                    <div class="card-footer text-center" style="background-color: whitesmoke;">
                        <div class="form-group">
                            <label for="">Harga</label>
                            <label for="">:</label>
                            <label for="">RP. {{ number_format( $val->harga) }}</label>
                        </div>
                        <input type="number" class="form-control" name="jumlah[]" id="tch3" min="1">
                    </div>
                </div>
            </div>
    @endforeach   
</form>
    </div> 

@endsection

{{-- modal pesanan --}}
<form action="{{ route('order.fixstore') }}" method="post">
    @csrf
<div class="modal fade" tabindex="-1" role="dialog" id="modalpesanan">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <div class="form-group">	
                  <h5 class="modal-title">No. Meja</h5>
                  <select name="no_meja" id="" class="form-control">
                      <?php $i = 1 ?>
                      @for($i = 1; $i < 10; $i++)
                          <option value="{{ $i }}">0{{ $i }}</option>
                      @endfor
                          <option value="10">10</option>
                  </select>
                <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
              </div>	
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <h5 class="modal-title">List Pesanan</h5>
              <table class="table table-bordered">
                  <thead>
                      <tr class="text-center">
                          <th>No</th>
                          <th>Pesanan</th>
                          <th>Harga</th>
                          <th>Jumlah</th>
                          <th>Subtotal</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($pesanan as $val)
                        <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ getMenu($val->id_menu)->nama_menu }}</td>
                        <td>Rp. {{ number_format(getMenu($val->id_menu)->harga) }}</td>
                        <td>{{ $val->jumlah }}</td>
                        <td>Rp. {{ number_format($val->sub_total)  }}</td>
                            <td>
                            <a href="{{ url('admin/order/destroy/'.$val->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                      @endforeach   
                  </tbody>
              </table>
              <div class="col-md-4">
                  <div class="form-group">
                        <label for="">Pesanan Atas Nama</label>
                        <label for="">:</label>
                        <input type="text" name="keterangan" class="form-control" required="">
                    </div>
              </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
              <input type="text" readonly="" name="id_order" class="form-control col-3 float-left" value="{{ $id_order }}">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-primary" onclick="$('#fixstore').submit()" >Pesan Sekarang</button>
            </div>
          </div>
        </div>
      </div>
    </div>
</form>