@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                    <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                    <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
                    <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
                    <a href="{{ url('admin/report') }}" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
                    <a href="{{ route('transaksi.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Transaksi</span></a>
            </li>
    </li>
@endsection

@section('body')
    <div class="card">
        <div class="card-header">
            <h4>Tamabah Menu</h4>
        </div>
        <div class="card-body">
        <a href="{{ route('menu.index') }}" class="btn btn-danger float-right">Back</a>
        <form action="{{ route('menu.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Kategori Menu</label>
                        <div class="input-group">
                            <select name="id_kategori" class="form-control" id="">
                               @foreach($kategori as $val)
                                <option value="{{ $val['id'] }}">{{ $val->kategori }}</option>
                               @endforeach
                            </select>
                            <div class="input-group-append">
                                <a href="{{ route('kategori.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Menu</label>
                        <label for="">:</label>
                        <input type="text" class="form-control" name="nama_menu">
                    </div>
                    <div class="form-group">
                        <label for="">Harga</label>
                        <label for="">:</label>
                        <input type="number" class="form-control" name="harga">
                    </div>
                    <div class="form-group">
                        <label for="">Gambar</label>
                        <label for="">:</label>
                        <input type="file" class="form-control" name="gambar">
                    </div>
                    <button class="btn btn-success">Simpan</button>
                </div>
            </div>
        </div>
    </form>
    </div>
    </div>
@endsection