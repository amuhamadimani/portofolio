@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                    <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                    <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
                    <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
                    <a href="{{ url('admin/report') }}" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
                    <a href="{{ route('transaksi.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Transaksi</span></a>
            </li>
    </li>
@endsection

@section('body')
        <div class="card">
        <div class="card-header">
            <h4>List Menu</h4>
        </div>
        <div class="card-body">
        <a href="{{ route('menu.create') }}" class="btn btn-primary float-right"><i class="fa fa-plus"></i>Tambah Menu</a>
        <br><br>    
        <table class="table table-bordered" id="dabel">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>Nama Menu</th>
                        <th>Harga</th>
                        <th>Status Menu</th>
                        <th>Gambar</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($menu as $val)
                    <tr class="text-center">
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $val->nama_menu }}</td>
                        <td>Rp. {{ number_format($val->harga)  }}</td>
                        <td>{{ $val->status_menu }}</td>
                        <td><img src="{{ asset('assets/img/menu/'.$val->gambar) }}" width="100" height="100" alt=""></td>
                        <td>
                        <a href="{{ url('admin/menu/edit/'.$val->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                        <a href="{{ url('admin/menu/destroy/'.$val->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
@endsection