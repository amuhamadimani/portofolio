@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Waiter</li>
    <li class="dropdown">
            <li class="dropdown">
            <a href="{{ Route('waiter.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
            <a href="" class="nav-link"><i class="fas fa-box"></i><span>Pesanan</span></a>
            <a href="{{ url('waiter/report') }}" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
            </li>
    </li>
@endsection

@section('body')
    <div class="card">
        <div class="card-header">
            <h4>Pesanan Siap Diantar</h4>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="dabel">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Id Order</td>
                        <td>No Meja</td>
                        <td>Atas Nama</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pesanan as $val)
                        <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $val->id_order }}</td>
                        <td>{{ $val->no_meja }}</td>
                        <td>{{ $val->keterangan }}</td>
                            <td>
                            <a href="{{ url('waiter/detail/'.$val->id_order) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr> 
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection