@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                <a href="" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
            <a href="" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
                <a href="" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
                <a href="" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
                <a href="" class="nav-link"><i class="fas fa-fire"></i><span>Transaksi</span></a>
            </li>
    </li>
@endsection

@section('body')
  <div class="card">
      <div class="card-header">
          <h4>Report</h4>
      </div>
      <div class="card-body">
        <div class="col-md-4">
                <div class="form-group">
                    <label for="">Tanggal Awal</label>
                    <label for="">:</label>
                    <label for="">{{ @$t1 }}</label>
                <input type="hidden" name="tanggal_awal" value="{{ @$t1 }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Tanggal Akhir</label>
                    <label for="">:</label>
                    <label for="">{{ @$t2 }}</label>
                    <input type="hidden" name="tanggal_awal" value="{{ @$t2 }}">
                </div>
            </div>
        <a href="{{ url('admin/report/pdftanggal/'.@$t1.'/'.@$t2) }}" target="_blank" class="btn btn-danger"><i class="fa fa-print"></i>Print PDF</a>
        <a href="{{ url('admin/report/exceltanggal/'.@$t1.'/'.@$t2) }}" target="_blank" class="btn btn-success"><i class="fa fa-print"></i>Export Excel</a>
        <br><br>
        <table class="table table-bordered" id="dabel">
              <thead class="text-center">
                  <tr>
                      <th>No</th>
                      <th>Nama Pemesan</th>
                      <th>Total Bayar</th>
                  </tr>
              </thead>
              <tbody class="text-center">
                  @foreach($data as $val)
                    <tr>
                    <td>{{ $loop->index +1 }}</td>
                    <td>{{ getUser($val->id_user) }}</td>
                    <td>{{ $val->total_bayar }}</td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
  </div>
@endsection