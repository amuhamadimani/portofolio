<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
		$date = date('Y-m-d');
        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        header("Content-type:   application/x-msexcel; charset=utf-8");
        header("Content-Disposition: attachment; filename=Laporan Transaksi-$date.xls");
	?>
	<h3>Laporan Semua Transaksi</h3>
	<table border="1" style="width: 100%;" align="center">
		<thead>
			<tr>
				<th>No</th>
				<th>Tanggal</th>
				<th>Nama Pemesan</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			@foreach($excel as $val)
				<tr>
					<td>{{ $loop->index+1 }}</td>
					<td>{{ $val->tanggal }}</td>
					<td>{{ getUser($val->id_user) }}</td>
					<td>{{ $val->total_bayar }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>