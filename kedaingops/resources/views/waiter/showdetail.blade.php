@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Waiter</li>
    <li class="dropdown">
            <li class="dropdown">
            <a href="{{ Route('waiter.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
            <a href="{{ url('waiter/show') }}" class="nav-link"><i class="fas fa-box"></i><span>Pesanan</span></a>
            <a href="{{ url('waiter/report') }}" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
            </li>
    </li>
@endsection

@section('body')
    <div class="card">
        <div class="card-header">
            <h4>Pesanan Siap Diantar</h4>
        </div>
        <div class="card-body">
            <table class="table table-bordered" id="dabel">
                <thead>
                    <tr class="text-center">
                        <th>No</th>
                        <th>No Meja</th>
                        <th>Pesanan</th>
                        <th>Atas Nama</th>
                        <th>Status Pesanan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pesanan as $val)
<form action="{{ url('waiter/update/'.$val->id_order) }}" method="post">
    @csrf
                        <tr class="text-center">
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $val->no_meja }}</td>
                        <td>{{ getMenu($val->id_menu)->nama_menu }}</td>
                        <td>{{ $val->keterangan }}</td>
                        <td>
                            @if($val->status_detail_order == 'siap dimasak')
                                <span class="badge badge-warning">{{ $val->status_detail_order }}</span>
                            @else
                                <span class="badge badge-info">{{ $val->status_detail_order }}</span>
                            @endif
                        </td>
                        <td><button type="submit" class="btn btn-success">Siap Diantar</button></td>
                        </tr> 
                    @endforeach
</form>
                </tbody>
            </table>
        </div>
    </div>
@endsection