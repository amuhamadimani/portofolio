@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                <a href="{{ route('report.index') }}" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
                <a href="{{ route('kasir.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Transaksi</span></a>
            </li>
    </li>
@endsection

@section('body')
   <div class="card">
       <div class="card-header">
       <h4>Pesanan Atas Nama : {{ $pesanan->keterangan }}</h4>
       <h4 class="ml-5">Dengan ID : {{ $pesanan->id_order }}</h4>
       </div>
       <div class="card-body">
       <form action="{{ url('admin/transaksi/store') }}" method="post">
        @csrf
           <table class="table table-bordered">
               <thead>
                   <tr class="text-center">
                       <th>No</th>
                       <th>Pesanan</th>
                       <th>Jumlah</th>
                       <th>Subtotal</th>
                   </tr>
               </thead>
               <tbody>
                   @foreach($detail as $val)
                   <tr class="text-center">
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ getMenu($val->id_menu)->nama_menu }}</td>
                        <td>{{ $val->jumlah }}</td>
                        <td>Rp. {{ number_format($val->sub_total) }}</td>
                    </tr>
                   @endforeach
               </tbody>
               <tfoot>
                   <tr class="text-center">
                       <td colspan="3"><h4>TOTAL</h4></td>
                   <td><input type="number" value="{{ $sum }}" id="total" name="total"  readonly class="form-control text-center"></td>
                   </tr>
                   <tr class="text-center">
                        <td colspan="3"><h4>BAYAR</h4></td>
                        <td><input type="number"  id="bayar"  class="form-control text-center"></td>
                    </tr>
                    <tr class="text-center">
                            <td colspan="3"><h4>KEMBALIAN</h4></td>
                            <td><input type="number" id="kembalian" readonly class="form-control text-center"></td>
                        </tr>
                        
               </tfoot>
           </table>
        <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
        <input type="hidden" name="id_order" value="{{ $pesanan->id_order }}">
           <button id="btnBayar" class="btn btn-success btn-lg btn-block">Bayar</button>
        </form>
       </div>
   </div>
@endsection

