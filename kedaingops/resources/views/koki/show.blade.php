@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                <a href="" class="nav-link"><i class="fas fa-fire"></i><span>Dapur</span></a>
            </li>
    </li>
@endsection

@section('body')
   <div class="card">
       <div class="card-header">
       <h4>Pesanan Atas Nama : {{ $pesanan->keterangan }}</h4>
       <h4 class="ml-5">Dengan ID : {{ $pesanan->id_order }}</h4>
       <input type="hidden" name="id_order" value="{{ $pesanan->id_order }}">
       </div>
       <div class="card-body">
       <a href="{{ Route('koki.index') }}" class="btn btn-danger float-right">Back</a>
       <br><br>
           <table class="table table-bordered">
               <thead>
                   <tr class="text-center">
                       <th>No</th>
                       <th>Pesanan</th>
                       <th>Jumlah</th>
                       <th>Status Pesanan</th>
                       <th>Action</th>
                   </tr>
               </thead>
               <tbody>
                   @foreach($detail as $val)
       <form action="{{ url('koki/update/'.$val->id_order) }}" method="post">
        @csrf
                   <tr class="text-center">
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ getMenu($val->id_menu)->nama_menu }}</td>
                        <td>{{ $val->jumlah }}</td>
                        <td>
                            @if($val->status_detail_order == 'siap dimasak')
                                <span class="badge badge-warning">{{ $val->status_detail_order }}</span>
                            @else
                                <span class="badge badge-info">{{ $val->status_detail_order }}</span>
                            @endif
                        </td>
                        <td><button class="btn btn-success btn-lg"><i class="fas fa-check"></i></button></td>
                    {{-- <td><a href="{{ url('koki/edit/'.$val->id_order.'/'.$val->id_menu) }}" class="btn btn-success"><i class="fa fa-check" ></i></a></td> --}}
                    </tr>
                   @endforeach
        </form>
               </tbody>
           </table>
       </div>
   </div>
@endsection

