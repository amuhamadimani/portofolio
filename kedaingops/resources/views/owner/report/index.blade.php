@extends('layouts.main');

@section('title','Kedaingops')

@section('sidebar-menu')
    <li class="menu-header">Admin</li>
    <li class="dropdown">
            <li class="dropdown">
                <a href="" class="nav-link"><i class="fas fa-list"></i><span>Report</span></a>
            </li>
    </li>
@endsection

@section('body')
<form action="{{ route('report.tanggal') }}" method="post">
@csrf
  <div class="card">
      <div class="card-header">
          <h4>Report</h4>
      </div>
      <div class="card-body">
          <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                      <label for="">Tanggal Awal</label>
                      <input type="date" name="tanggal_awal" class="form-control">
                  </div>
              </div>
              <div class="col-md-4">
                  <label for="">Tanggal Akhir</label>
                  <input type="date" name="tanggal_akhir" class="form-control">
              </div>
              <div class="col-md-4">
                  <button class="btn btn-primary btn-lg" style="height:70px;">Lihat</button>
              </div>
          </div>
          <hr>
        <a href="{{ url('admin/report/pdf') }}" target="_blank" class="btn btn-danger"><i class="fa fa-print"></i>Print PDF</a>
        <a href="{{ url('admin/report/excel') }}" target="_blank" class="btn btn-success"><i class="fa fa-print"></i>Export Excel</a>
        <br><br>  
        <table class="table table-bordered" id="dabel">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Nama Pemesan</th>
                      <th>Total Bayar</th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($report as $val)
                    <tr>
                    <td>{{ $loop->index +1 }}</td>
                    <td>{{ $val->tanggal }}</td>
                    <td>{{ getUser($val->id_user) }}</td>
                    <td>{{ $val->total_bayar }}</td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
  </div>
</form>
@endsection