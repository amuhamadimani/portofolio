<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css')}}">

</head>
<body>
    <style>
        @media print{
            
        }
    </style>
	<h3>Laporan Semua Transaksi</h3>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Tanggal</th>
				<th>Nama Pemesan</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			@foreach($pdf as $val)
				<tr>
					<td>{{ $loop->index+1 }}</td>
					<td>{{ $val->tanggal }}</td>
					<td>{{ getUser($val->id_user) }}</td>
					<td>{{ $val->total_bayar }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
<script>
    window.print();
</script>