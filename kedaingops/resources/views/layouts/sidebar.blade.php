<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand sidebar-gone-show"><a href="dashboard-general.html">Stisla</a></div>
      <ul class="sidebar-menu">
        @yield('sidebar-menu')
        
      </ul>
    </aside>
  </div>