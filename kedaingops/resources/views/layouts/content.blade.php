<div class="main-content">
	<section class="section">
	  <div class="section-header">
	  	<h1>Kedai Ngops | <i>Enjoy-in Aja</i></h1>
	    <div class="section-header-breadcrumb">
	      @yield('breadcrumb')
	    </div>
	  </div>

	  <div class="section-body">
	    @yield('body')
	  </div>
	</section>
</div>