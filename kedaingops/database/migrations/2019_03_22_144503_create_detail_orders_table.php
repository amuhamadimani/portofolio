<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->UnsignedInteger('id_order');
            $table->integer('id_menu');
            $table->integer('jumlah');
            $table->integer('sub_total');
            $table->string('keterangan');
            $table->enum('status_detail_order',[
                'pending',
                'sedang dimasak',
                'selesai dimasak'
            ])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_orders');
    }
}
