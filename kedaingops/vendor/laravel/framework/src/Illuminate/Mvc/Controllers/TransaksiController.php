<?php

namespace App\Http\Controllers;

use App\Models\transaksi;
use App\Models\order;
use App\Models\pesanan;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = order::all();

        return view('admin.transaksi.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        transaksi::create([
            'id_user' => $request->id_user,
            'id_order' => $request->id_order,
            'tanggal' => date('Y-m-d'),
            'total_bayar' => $request->total,
        ]);

        order::where('status_order', 'belum dibayar')->update([
            'status_order' => 'dibayar',
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = pesanan::where('id_order', $id)->get();
        $get = pesanan::select('pesanans.id_order')->where('id_order', $id)->first();
        $total = pesanan::where('id_order', $id)->sum('sub_total');

        return view('admin.transaksi.show', compact('show','total','get'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(transaksi $transaksi)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, transaksi $transaksi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(transaksi $transaksi)
    {
        //
    }
}
