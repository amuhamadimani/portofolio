<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\menu;
use App\Models\order;
use App\Models\pesanan;
use App\Models\transaksi;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $a = pesanan::select('id')->latest()->value('id');
        // $no = substr($a, 3);
        // $id = sprintf('%03s', $no+1);
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        $data = menu::paginate(6);
        
        $menu = pesanan::where('status', '0')->get();
        $appetizer = pesanan::where('id_kategori', '1');
        $maincourse = pesanan::where('id_kategori', '2');
        $desert = pesanan::where('id_kategori', '3');
        $drink = pesanan::where('id_kategori', '4');
        return view('admin.order.index', compact('data','menu','appetizer', 'maincourse','desert','drink', 'id_order'));
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Validator($request->all(),[
            'jumlah' => 'required',
        ]);
        // dd($request);

        for($i = 0; $i < count($request->jumlah); $i++){
            if($request->jumlah[$i] != null) {
                pesanan::create([
                    'id_menu' => $request->id_menu[$i],
                    // 'id_order' => $request->id_order,
                    'jumlah' => $request->jumlah[$i],
                    'sub_total' => $request->jumlah[$i] * getMenu($request->id_menu[$i])->harga 
                ]);
            }        
        }
        return redirect()->back();
    }

    public function fixstore(Request $request)
    {
        $rules = Validator($request->all(),[
            'id_user' => 'required',
            'nama' => 'required',
        ]);

        // $total = pesanan::count('sub_total')->where('id_order', $request->id_order);

        order::create([
            'id_order' => $request->id_order,
            'id_user' => $request->id_user,
            'no_meja' => $request->no_meja,
            'keterangan' => $request->keterangan,
        ]);

        pesanan::where('status', 0)->update([
            'id_order' => $request->id_order,
            'keterangan' => $request->keterangan,
            'status' => 1
        ]);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }   
}
    