<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kategori;
use App\Models\menu;
use Validator;
use Storage;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = menu::all();
        return view('admin/menu/index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $kategori = kategori::all();
        return view('admin/menu/create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Validator::make($request->all(),[
            'id_kategori' => 'required',
            'nama_menu' => 'required',
            'harga' => 'required',
            
            'keterangan' => 'required'
        ]);

        $do = new menu($request->all());
        //$do->gambar = $request->file('gambar')->store('public/app/menu');

        $gambar = $request->file('gambar');

        $filenameWithExt = $gambar->getClientOriginalName();
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        $extention = $gambar->getClientOriginalExtension();
        $fileNameToStore = $filename . '_' . time() . '.' . $extention;
        $path = $gambar->move(public_path() . '/assets/img/menu', $fileNameToStore);

        $do->gambar = $fileNameToStore;

        $do->save();

        return redirect('/admin/menu')->with('success','Data tersimpan!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = menu::where('id', $id)->first();
        $kategori = kategori::all();
        return view('admin/menu/edit', compact('edit','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        menu::where('id', $id)->delete();
        return redirect()->back()->with('error','Data successfuly deleted !');
    }
}
