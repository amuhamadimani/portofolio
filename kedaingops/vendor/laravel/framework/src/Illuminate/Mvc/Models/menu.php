<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\kategori;

class menu extends Model
{
    protected $table = 'menus';
    protected $fillable = [
    	'id_kategori',
    	'nama_menu',
    	'harga',
    	'gambar',
    ];

    public function kategori(){
    	return $this->hasOne('App\Models\kategori','id','id_kategori');
    }
}
