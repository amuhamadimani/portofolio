<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});;


Auth::routes();
	
Route::get('/home', 'HomeController@index')->name('home');
 

Route::group(['prefix' => 'admin'], function(){
	Route::get('/', ['as' =>'admin.index', 'uses'=>'AdminController@index']);
	
	Route::group(['prefix' => 'menu'], function(){
		Route::get('/', ['as' =>'menu.index', 'uses'=>'MenuController@index']);
		Route::get('/create', ['as' =>'menu.create', 'uses'=>'MenuController@create']);
		Route::post('/store', ['as' =>'menu.store', 'uses'=>'MenuController@store']);
		Route::get('/edit/{id}', ['as' =>'menu.edit', 'uses'=>'MenuController@edit']);
		Route::patch('/update/{id}', ['as' =>'menu.update', 'uses'=>'MenuController@update']);
		Route::get('/destroy/{id}', ['as' =>'menu.destroy', 'uses'=>'MenuController@destroy']);
	});
	Route::group(['prefix' => 'order'], function(){
		Route::get('/', ['as' =>'order.index', 'uses'=>'OrderController@index']);
		Route::get('/create', ['as' =>'order.create', 'uses'=>'OrderController@create']);
		Route::post('/store', ['as' =>'order.store', 'uses'=>'OrderController@store']);
		Route::post('/fixstore', ['as' =>'order.fixstore', 'uses'=>'OrderController@fixstore']);
		Route::get('/edit/{id}', ['as' =>'order.edit', 'uses'=>'OrderController@edit']);
		Route::patch('/update/{id}', ['as' =>'order.update', 'uses'=>'OrderController@update']);
		Route::delete('/destroy/{id}', ['as' =>'order.destroy', 'uses'=>'OrderController@destroy']);
	});
	Route::group(['prefix' => 'level'], function(){
		Route::get('/', ['as' =>'level.index', 'uses'=>'LevelController@index']);
		Route::get('/create', ['as' =>'level.create', 'uses'=>'LevelController@create']);
		Route::post('/store', ['as' =>'level.store', 'uses'=>'LevelController@store']);
		Route::get('/edit/{id}', ['as' =>'level.edit', 'uses'=>'LevelController@edit']);
		Route::patch('/update/{id}', ['as' =>'level.update', 'uses'=>'LevelController@update']);
		Route::get('/destroy/{id}', ['as' =>'level.destroy', 'uses'=>'LevelController@destroy']);
	});
	Route::group(['prefix' => 'kategori'], function(){
		Route::get('/create', ['as' =>'kategori.create', 'uses'=>'KategoriController@create']);
		Route::post('/store', ['as' =>'kategori.store', 'uses'=>'KategoriController@store']);
		Route::get('/edit/{id}', ['as' =>'kategori.edit', 'uses'=>'KategoriController@edit']);
		Route::patch('/update/{id}', ['as' =>'kategori.update', 'uses'=>'KategoriController@update']);
		Route::get('/destroy/{id}', ['as' =>'kategori.destroy', 'uses'=>'KategoriController@destroy']);
	});
	Route::group(['prefix' => 'meja'], function(){
		Route::get('/', ['as' =>'meja.index', 'uses'=>'MejaController@index']);
		// Route::get('/create', ['as' =>'meja.create', 'uses'=>'MejaController@create']);
		Route::post('/store', ['as' =>'meja.store', 'uses'=>'MejaController@store']);
		Route::get('/edit/{id}', ['as' =>'meja.edit', 'uses'=>'MejaController@edit']);
		Route::patch('/update/{id}', ['as' =>'meja.update', 'uses'=>'MejaController@update']);
	});
	Route::group(['prefix' => 'report'], function(){
		Route::get('/', ['as' =>'report.index', 'uses'=>'ReportController@index']);
		Route::get('/excel', ['as' =>'report.excel', 'uses'=>'ReportController@excel']);
		Route::get('/exceltanggal', ['as' =>'report.excel', 'uses'=>'ReportController@exceltanggal']);
		Route::post('/tanggal', ['as' =>'report.tanggal', 'uses'=>'ReportController@tanggal']);
	});

	Route::group(['prefix' => 'transaksi'], function(){
		Route::get('/', ['as' =>'transaksi.index', 'uses'=>'TransaksiController@index']);
		Route::get('/show/{id}', ['as' =>'transaksi.show', 'uses'=>'TransaksiController@show']);
		Route::post('/store', ['as' =>'transaksi.store', 'uses'=>'TransaksiController@store']);
		Route::get('/edit/{id}', ['as' =>'transaksi.edit', 'uses'=>'TransaksiController@edit']);
		Route::patch('/update/{id}', ['as' =>'transaksi.update', 'uses'=>'TransaksiController@update']);
	});
});

Route::group(['prefix'=>'waiter'], function(){
	Route::get('/', ['as' => 'waiter.index', 'uses'=>'WaiterController@index']);
});

Route::group(['prefix'=>'pelanggan'], function(){
	Route::get('/dashboard', ['as' => 'pelanggan.index', 'uses'=>'PelangganController@index']);
});