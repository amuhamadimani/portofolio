@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('body')

	<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Tanggal Awal</label>
					<label for="">:</label>
					<label for="">{{ @$t1 }}</label>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Tanggal Akhir</label>
					<label for="">:</label>
					<label for="">{{ @$t2 }}</label>
				</div>
			</div>
	</div>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Pemesan</th>
				<th>Total Beli</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $val)
			<tr>
				<td>{{ $loop->index+1 }}</td>
				<td>{{ getUser($val->id_user) }}</td>
				<td>{{ $val->total_bayar }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

@endsection