<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title')</title>
  <style>
    #tch3{
      height: 35px;
    }
  </style>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('dist/modules/izitoast/css/iziToast.min.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/modules/dropify/dist/css/dropify.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/modules/prism/prism.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/modules/chocolat/dist/css/chocolat.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/modules/datatables/datatables.min.css')}}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('dist/css/style.css')}}">
</head>

<body class="layout-2">
  <div id=" ">
    <div class="main-wrapper">
      <div class="navbar-bg"></div> 
      @include('layouts.navbar')
      
      @include('layouts.sidebar')

      <!-- Main Content -->
      @include('layouts.content')
      
      
  <!-- General JS Scripts -->
  <script src="{{ asset('dist/modules/jquery.min.js')}}"></script>
  <script src="{{ asset('dist/modules/popper.js')}}"></script>
  <script src="{{ asset('dist/modules/tooltip.js')}}"></script>
  <script src="{{ asset('dist/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('dist/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{ asset('dist/modules/moment.min.js')}}"></script>
  <script src="{{ asset('dist/js/stisla.js')}}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('dist/modules/sticky-kit.js')}}"></script>
  <script src="{{ asset('dist/modules/izitoast/js/iziToast.min.js')}}"></script>
  <script src="{{ asset('dist/js/page/modules-toastr.js')}}"></script>
  <script src="{{ asset('dist/modules/dropify/dist/js/dropify.min.js')}}"></script>
  <script src="{{ asset('dist/modules/ckeditor/js/ckeditor.js')}}"></script>
  <script src="{{ asset('dist/modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')}}"></script>
  <script src="{{ asset('dist/modules/chocolat/dist/js/jquery.chocolat.min.js')}}"></script>
  <script src="{{ asset('dist/modules/datatables/datatables.min.js')}}">
    
  </script>


  @include('toast.notification')

  <script>
    $(function(){
        // Set up the number formatting.
          $('#bayar').keyup(function(){
            var total = parseInt($('#total').val()); 
            var bayar = parseInt($('#bayar').val()); 
            var kembali = bayar - total;

            $("#kembalian").val(kembali);

            if (kembali < 0  || $("#kembalian").val() == '') {
              $("btnBayar").attr('disabled', 'disabled');
            }else{
              $("btnBayar").removeAttr('disabled');
            }
          });
        $('#number_container').slideDown('fast');
        
        $('#price').on('change',function(){
          console.log('Change event.');
          var val = $('#price').val();
          $('#the_number').text( val !== '' ? val : '(empty)' );
        });
        
        $('#price').change(function(){
          console.log('Second change event...');
        });
        
        $('#price').number(true);
        
        
        // Get the value of the number for the demo.
        $('#get_number').on('click',function(){
          
          var val = $('#price').val();
          
          $('#the_number').text( val !== '' ? val : '(empty)' );
        });
      });
    
      $('.dropify').dropify();

      $(window).on('load', function() {
              // classic editor
              ClassicEditor.create(document.querySelector('.ckeditor'))
                  .catch(error => {
                      console.error(error);
                  });
          });

      $("input[id='tch3']").TouchSpin();

      $('#dabel').DataTable();

      $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
      })
  </script>

  <script src="{{ asset('dist/js/number/jquery.number.min.js') }}"></script>
  <!-- Page Specific JS File -->
  
  <!-- Template JS File -->
  <script src="{{ asset('dist/js/scripts.js')}}"></script>
  <script src="{{ asset('dist/js/custom.js')}}"></script>
</body>
</html>