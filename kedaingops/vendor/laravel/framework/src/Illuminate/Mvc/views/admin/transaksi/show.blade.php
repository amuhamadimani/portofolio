@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item">Transaksi</div>
@endsection

@section('body')

<div class="card">
	<div class="card-header">
		<h4>List Order</h4>
	</div>
	<div class="card-body">
		<table class="table table-bordered">
			<thead class="text-center">
				<tr>
					<th>Id Order</th>
					<th>Nama Menu</th>
					<th>Jumlah</th>
					<th>Subtotal</th>
				</tr>
			</thead>
			<tbody>
			<form action="{{ route('transaksi.store') }}" method="post">
			@csrf
				@foreach($show as $data)
					<tr class="text-center">
						<td>{{ $data->id_order }}</td>
						<td>{{ getMenu($data->id_menu)->nama_menu }}</td>
						<td>{{ $data->jumlah }}</td>
						<td>Rp. {{ number_format($data->sub_total) }}</td>
					</tr>
				@endforeach
				<tr class="text-center">
					<td colspan="3">
						<h4>Total</h4>
					</td>
					<td>
						<input type="text" readonly="" name="total" value="{{ $total }}" id="total" class="form-control">
					</td>
				<tr class="text-center">
					<td colspan="3">
						<h4>Bayar</h4>
					</td>
					<td>
						<input type="number" id="bayar" required="" class="form-control">
					</td>
				</tr>
				<tr class="text-center">
					<td colspan="3">
						<h4>Kembalian</h4>
					</td>
					<td>
						<input type="number" id="kembalian" readonly="" class="form-control">
					</td>
				</tr>
			</tbody>
		</table>
		<input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
		<input type="hidden" name="id_order" value="{{ $get->id_order }}">
		<button class="btn btn-success btn-lg float-right btn-block" id="btnBayar">Bayar</button>
		</form>
	</div>
</div>

@endsection
