<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	
	<?php
		$date = date('Y-m-d');
		header("Content-type : application/vnd-ms-excel");
		header("Content-Disposition : attachment; filename=Laporan Transaksi - $date.xls");
	?>
	<h3>Laporan Per Tanggal</h3>
	<table border="1" style="width: 100%;" align="center">
		<thead>
			<tr>
				<th>No</th>
				<th>Tanggal</th>
				<th>Nama Pemesan</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			@foreach($excel as $data)
				<tr>
					<td>{{ $loop->index+1 }}</td>
					<td>{{ $data->tanggal }}</td>
					<td>{{ getUser($data->id_user) }}</td>
					<td>{{ $data->total_bayar }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>