@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item">Transaksi</div>
@endsection

@section('body')

<div class="card">
	<div class="card-header">
		<h4>List Order</h4>
	</div>
	<div class="card-body">
		<table class="table table-bordered" id="dabel">
			<thead class="text-center">
				<tr>
					<th>No</th>
					<th>ID ORDER</th>
					<th>Atas Nama</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($order as $val)
					<tr class="text-center">
						<td>{{ $loop->index+1 }}</td>
						<td>{{ $val->id_order }}</td>
						<td>{{ $val->keterangan }}</td>
						<td>
							<a href="{{ url('admin/transaksi/show/'.$val->id_order) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
	
@endsection