@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></div>
  	<div class="breadcrumb-item">Tambah Menu</div>
@endsection

@section('body')
	<div class="card">
		<div class="card-header">
			<h4>Input Menu</h4>
		</div>
		<form action="{{ url('admin/level/update/'.$edit['id']) }}" method="post">
		{{ @csrf_field() }}
		{{ @method_field('PATCH') }}
		<div class="card-body">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Nama Level</label>
						<input type="text" name="nama_level" value="{{ $edit['nama_level'] }}" class="form-control">
					</div>
					<button class="btn btn-success">Update</button>
				</div>
				<div class="col-md-8">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Level</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $val)
								<tr>
									<td>{{ $loop->index + 1 }}</td>
									<td>{{ $val->nama_level }}</td>
									<td>
										<a href="{{ url('admin/level/edit/'.$val->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection