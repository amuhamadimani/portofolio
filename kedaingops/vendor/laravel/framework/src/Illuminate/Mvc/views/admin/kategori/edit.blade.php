@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></div>
 	<div class="breadcrumb-item"><a href="{{ route('menu.create') }}">Tambah Menu</a></div>
  	<div class="breadcrumb-item">Kategori</div>
@endsection

@section('body')
	<div class="card">
		<div class="card-header">
			<h4>Input Menu</h4>
		</div>
		<div class="card-body">
			<form action="{{ url('admin/kategori/update/'.$edit['id']) }}" method="post">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<div class="row">
				<div class="col-md-5">
					<div class="form-group">
						<label for="">Kategori Menu</label>
						<input type="text" name="kategori" value="{{ $edit['kategori']}}" class="form-control">
					</div>
					<button class="btn btn-success">Update</button>
				</div>
				<div class="col-md-7">
					<table class="table table bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Kategori</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($kategori as $data)
								<tr>
									<th>{{ $loop->index + 1 }}</th>
									<th>{{ $data->kategori }}</th>
									<th>
										<div class="button-group">
											<a href="{{ url('admin/kategori/edit/'.$data->id) }}" class="btn btn-icon icon-left btn-info"><i class="fa fa-edit"></i></a>
										</div>
									</th>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			</form>
		</div>
	</div>
@endsection