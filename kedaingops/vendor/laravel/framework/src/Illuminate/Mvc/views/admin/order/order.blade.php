@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item"><a href="">Order</a></div>
@endsection

@section('body')
</style>
	<div class="card">
		<div class="card-header">
			<div class="col-md-4">
				<h4>Order</h4>
			</div>
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<ul class="pagination">
					<li class="page-item ">
		              <a class="page-link" href="#" tabindex="-1"><i class="fa fa-angle-double-left"></i></a>
		            </li>
		            <li class="page-item active"><a class="page-link" href="{{ route('order.pagging',[0]) }}">1</a></li>
		            <li class="page-item">
		              <a class="page-link" href="{{ route('order.pagging',[0]) }}">2 <span class="sr-only">(current)</span></a>
		            </li>
		            <li class="page-item"><a class="page-link" href="#">3</a></li>
		            <li class="page-item">
		              <a class="page-link" href="{{ route('order.paging', [$next+6]) }}"><i class="fa fa-angle-double-right"></i></a>
		            </li>
		        </ul>

			</div>
		</div>
	</div>
	<form action="{{ url('admin/order/store') }}" method="post">
	{{ csrf_field() }}
		<div class="row">
	@foreach($data as $val)
			<div class="col-md-4">
				<div class="card">
					<div class="card-header" style="background-color: whitesmoke;">
						<h4>{{ $val->nama_menu }}</h4>
					</div>
					<div class="card-body text-center">
                      <img alt="image" src="{{ asset('assets/img/menu/'.$val->gambar) }}" width="150">
					</div>
					<div class="card-footer">
						<div class="form-group">
							<label for="">Harga</label>
							<label for="" class="ml-5">:</label>
							<label for="">Rp.{{ number_format($val->harga) }}</label>
						</div>
						<div class="form-group">
							<label for="">Keterangan</label>
							<label for="" class="ml-3">:</label>
							<label for="">{{ $val->keterangan }}</label>
						</div>
					</div>
				</div>
			</div>
	@endforeach
		</div>
	</form>
	<div class="card" align="center">
		<div class="card-body">	
			<ul class="pagination col-4">
	            <li class="page-item disabled">
	              <a class="page-link" href="#" tabindex="-1"><i class="fa fa-angle-double-left"></i></a>
	            </li>
	            <li class="page-item active"><a class="page-link" href="#">1</a></li>
	            <li class="page-item">
	              <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
	            </li>
	            <li class="page-item"><a class="page-link" href="#">3</a></li>
	            <li class="page-item">
	              <a class="page-link" href="#"><i class="fa fa-angle-double-right"></i></a>
	            </li>
	        </ul>
		</div>
	</div>

@endsection

{{-- <div class="card">
	<div class="card-body"style="background-color: whitesmoke;">
		<img src="{{ asset('assets/img/menu/'.$val->gambar) }}" alt="" width="200" height="100">
	</div>
</div>
<div class="card-footer">
	<div class="form-group">
		<label for="">Nama Menu</label>
		<label for="" class="col-1">:</label>
		<label for="">{{ $val->nama_menu }}</label>
	</div>
	<div class="form-group">
		<label for="" >Harga</label>
		<label for="" class="col-2">:</label>
		<label for="">RP.{{ number_format($val->harga) }}</label>
	</div>
	<div class="form-group">
		<label for="">Keterangan</label>
		<label for="">:</label>
		<label for="">{{ $val->keterangan }}</label>
	</div>
</div>
<br> --}}