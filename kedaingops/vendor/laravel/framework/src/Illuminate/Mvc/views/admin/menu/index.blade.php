@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Main Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></div>
@endsection

@section('body')
	<div class="card">
		<div class="card-header">
			<h4>List Makanan</h4>
		</div>
		<div class="card-body">
			<a href="{{ route('menu.create') }}" class="btn btn-icon icon-left btn-primary float-right">
				<i class="fa fa-plus"></i>
				Tambah Menu
			</a>
			<br><br><br>
			<table class="table table-bordered table-striped" id="dabel">
				<thead>
					<tr>
						<th>No</th>
						<th>Kategori Menu</th>
						<th>Nama Menu</th>
						<th>Harga</th>
						<th>Status Makanan</th>
						<th>Gambar</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $val)
					<tr>
						<td>{{ $loop->index + 1 }}</td>
						<td>{{ $val->kategori->kategori }}</td>
						<td>{{ $val->nama_menu }}</td>
						<td>Rp. {{ number_format($val->harga) }}</td>
						<td>{{ $val->status_menu }}</td>
						<td><img src="{{ asset('assets/img/menu/'.$val->gambar) }}" width="100" alt=""></td>
						<td>
							<a href="{{ url('admin/menu/edit/'.$val->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
							<a href="{{ url('admin/menu/destroy/'.$val->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection