@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item">Transaksi</div>
@endsection

@section('body')

<div class="card">
	<div class="card-header">
		<h4>Report</h4>
	</div>
	<div class="card-body">
		<form action="{{ route('report.tanggal') }}" method="post">
		@csrf
		<h5>Report Berdasarkan Tanggal</h5>
		<br>
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Tanggal Awal</label>
					<input type="date" name="tanggal_awal" class="form-control">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Tanggal Akhir</label>
					<input type="date" name="tanggal_akhir" class="form-control">
				</div>
			</div>
		</div>
		<button class="btn btn-info">Lihat Report</button>
	</div>
	</form>
	<hr>
	<h4 class="text-center">Laporan Seluruh Transaksi</h4>
	<div class="button-group">
		<button class="btn btn-danger col-2"><i class="fa fa-print"></i>Print Laporan</button>
		<a href="{{ url('admin/report/excel') }}" target="_blank" class="btn btn-success col-2"><i class="fa fa-print"></i>Export Excel</a>
	</div>
	<br>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Tanggal</th>
				<th>Nama Pemesan</th>
				<th>Total Beli</th>
			</tr>
		</thead>
		<tbody>
			@foreach($report as $data)
				<tr>
					<td>{{ $loop->index+1 }}</td>
					<td>{{ $data->tanggal }}</td>
					<td>{{ getUser($data->id_user) }}</td>
					<td>{{ $data->total_bayar }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
	
@endsection