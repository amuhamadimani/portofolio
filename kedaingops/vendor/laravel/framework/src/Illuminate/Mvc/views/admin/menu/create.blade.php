@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></div>
  	<div class="breadcrumb-item">Tambah Menu</div>
@endsection

@section('body')
	<div class="card">
		<div class="card-header">
			<h4>Input Menu</h4>
		</div>
		<form action="{{ url('admin/menu/store') }}" enctype="multipart/form-data" method="post">
		{{ @csrf_field() }}
		<div class="card-body">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<label for="">Kategori Menu</label>
						<div class="input-group">
							<select name="id_kategori" class="form-control" id="">
								@foreach($kategori as $data)
									<option value="{{ $data['id'] }}">{{ $data->kategori }}</option>
								@endforeach
							</select>
							<div class="input-group-append">
								<a href="{{ route('kategori.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="">Nama Menu</label>
						<input type="text" name="nama_menu" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Harga</label>
						<input type="text" name="harga" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Gambar</label>
						<input type="file" name="gambar" class="form-control ">
					</div>
					<div class="form-group">
						<label for="">Ketrangan</label>
						<input type="text" name="keterangan" class="form-control">
					</div>
					<button class="btn btn-success">Simpan</button>
				</div>
			</div>
		</div>
		</form>
	</div>
@endsection