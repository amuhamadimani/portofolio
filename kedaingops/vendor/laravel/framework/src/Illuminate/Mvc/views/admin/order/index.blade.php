@extends('layouts.main')

@section('title', 'Kedai Ngops')

@section('sidebar-menu')
	<li class="menu-header">Mains Module</li>
    <li class="dropdown">
      <a href="{{ route('admin.index') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      <a href="{{ route('menu.index') }}" class="nav-link"><i class="fas fa-utensils"></i><span>Menu</span></a>
      <a href="{{ route('order.index') }}" class="nav-link"><i class="fas fa-box"></i><span>Order</span></a>
      <a href="{{ route('level.index') }}" class="nav-link"><i class="fas fa-layer-group"></i><span>Level</span></a>
    </li>
@endsection

@section('breadcrumb')
	<div class="breadcrumb-item active"><a href="{{ route('admin.index') }}">Dashboard</a></div>
 	<div class="breadcrumb-item">Order</div>
@endsection

@section('body')
	<div class="card">
		<div class="card-header">
			<div class="col-md-4">
			</div>
			<div class="col-md-6">
				
			</div>
			<div class="col-md-2">
					{{ $data->links() }}
				{{-- <ul class="pagination">
		            <li class="page-item active"><a class="page-link" href="#">1</a></li>
		            <li class="page-item">
		              <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
		            </li>
		            <li class="page-item"><a class="page-link" href="#">3</a></li>
		            <li class="page-item">
		              <a class="page-link" href="{{ route('order.paging', [6]) }}"><i class="fa fa-angle-double-right"></i></a>
		            </li>
		        </ul> --}}

			</div>
		</div>
	</div>
	<div align="center">
		<button class="btn btn-lg btn-icon btn-warning" data-toggle="modal" data-target="#modalpesanan"><i class="fa fa-shopping-cart"></i></button>
	</div>
	<br>
	<form action="{{ url('admin/order/store') }}" method="post">
	{{ csrf_field() }}
	{{-- <input type="hidden" name="id_order" class="form-control col-4" value="{{ $id_order }}" readonly=""> --}}
		<div class="row">
	<button class="btn btn-info w-5 btn-lg" style="position: fixed; top: 300px; right: 0; height: 50px; z-index: 1" type="submit">Pesan</button>

	@foreach($data as $val)
			<div class="col-md-4">
				<div class="card">
					<div class="card-header " style="background-color: whitesmoke;">
						<h4>{{ $val->nama_menu }}</h4>
						<input type="hidden" name="id_menu[]" value="{{ $val['id'] }}">
					</div>
					<div class="card-body text-center">
                    	<div class="chocolat-parent">
                    		<a href="{{ asset('assets/img/menu/'.$val->gambar) }}" class="chocolat-image">
	                    		<div data-crop-image="150">
	                    			<img alt="image" src="{{ asset('assets/img/menu/'.$val->gambar) }}" width="150" height="150">
		                        </div>
	                    	</a>
                    	</div>
					</div>
					<div class="card-footer">
						<div class="form-group">
							<label for="">Harga</label>
							<label for="" class="ml-5">:</label>
							<label for="" name="harga">Rp.{{ number_format($val->harga) }}</label>
						</div>
						<div class="form-group">
							<label for="">Keterangan</label>
							<label for="" class="ml-3">:</label>
							<label for="" >{{ $val->keterangan }}</label>
						</div>
						<div class="form-group" align="center">
							<input type="number" min="0" name="jumlah[]" id="tch3" class="form-control">
							<br>
						</div>
					</div>
				</div>
			</div>
	@endforeach
		</div>
	</form>
	<div class="card">
		<div class="card-body ">
			<div class="ml-5">{{ $data->links() }}</div>	
			
		</div>
	</div>

@endsection

{{-- Modal pesanan --}}
<form action="{{ route('order.fixstore') }}" id="fixstore" method="post">
{{ csrf_field() }}
	<div class="modal fade" tabindex="-1" role="dialog" id="modalpesanan">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <div class="form-group">	
					<h5 class="modal-title">No. Meja</h5>
					<select name="no_meja" id="" class="form-control">
						<?php $i = 1 ?>
						@for($i = 1; $i < 10; $i++)
							<option value="{{ $i }}">0{{ $i }}</option>
						@endfor
							<option value="10">10</option>
					</select>
                </div>	
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
              	<h5 class="modal-title">List Pesanan</h5>
                <table class="table table-bordered">
                	<thead>
                		<tr class="text-center">
                			<th>No</th>
                			<th>Pesanan</th>
                			<th>Harga</th>
                			<th>Jumlah</th>
                			<th>Subtotal</th>
                			<th>Action</th>
                		</tr>
                	</thead>
                	<tbody>
                		@foreach($menu as $data)
                		<tr>
                			<td>{{ $loop->index + 1 }}</td>
                			<td>{{ getMenu($data->id_menu)->nama_menu }}</td>
                			<td>Rp.{{ number_format(getMenu($data->id_menu)->harga) }}</td>
                			<td>{{ $data->jumlah }}</td>
                			<td id="sub_total">Rp.{{ number_format($data->sub_total) }}</td>
                			<td class="text-center">
                				<a href="" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                			</td>
                		</tr>
                		@endforeach
                	</tbody>
                </table>
                <div class="col-md-4">
                	<div class="form-group">
	              		<label for="">Pesanan Atas Nama</label>
	              		<label for="">:</label>
	              		<input type="text" name="keterangan" class="form-control" required="">
	              	</div>
                </div>
              </div>
              <div class="modal-footer bg-whitesmoke br">
                <input type="text" readonly="" name="id_order" class="form-control col-3 float-left" value="{{ $id_order }}">
                <button typ	e="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" onclick="$('#fixstore').submit()" >Pesan Sekarang</button>
              </div>
            </div>
          </div>
        </div>
      </div>
		{{-- <input type="text" name="id_order" value="{{ $id_order }}" > --}}
	  	<input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
	  	<script>
	  		
	  	</script>
</form>