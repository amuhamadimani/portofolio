<?php

namespace App\Imports;

use App\Models\kategori;
use Maatwebsite\Excel\Concerns\ToModel;

class KategoriImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new kategori([
            'kategori' => $row[1],
        ]);
    }
    public function headings(): array
    { 
        return [
            '#',
            'Kategori',
            'Created at',
            'Updated at'
        ];
    }
}
