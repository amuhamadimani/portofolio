<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class waiter extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'id_order',
        'no_meja',
        'id_user',
        'keterangan',
    ];
}
