<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    protected $table = 'transaksis';
    protected $fillable = [
        'id_user',
        'id_order',
        'tanggal',
        'total_bayar'
    ];
}
