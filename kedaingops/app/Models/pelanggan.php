<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pelanggan extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'id_order',
        'no_meja',
        'id_user',
        'keterangan',
    ];
}
