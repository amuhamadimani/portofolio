<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\menu;

class pesanan extends Model
{
    protected $table = 'pesanans';
    protected $fillable = [
    	'id_menu',
    	// 'id_order',	
    	'jumlah',
    	'sub_total'
    ];

}
