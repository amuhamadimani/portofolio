<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class detail_order extends Model
{
    protected $table = 'detail_orders';
    protected $fillable = [
        'id_menu',
        'jumlah',
        'sub_total',
    ];
}
