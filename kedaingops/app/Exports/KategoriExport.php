<?php

namespace App\Exports;

use App\Models\kategori;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class KategoriExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return kategori::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Kategori',
            'Created at',
            'Updated at'
        ];
    }
}
