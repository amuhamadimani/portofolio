<?php

namespace App\Http\Controllers;

use App\Models\menu;
use App\Models\kategori;
use Illuminate\Http\Request;
use Validator;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $menu = menu::all();
        return view('admin.menu.index', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $kategori = kategori::all();
        return view('admin.menu.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Validator::make($request->all(),[
            'id_kategori' => 'required',
            'nama_menu' => 'required',
            'harga' => 'required'
        ]);

        $do = new menu($request->all());
        //$do->gambar = $request->file('gambar')->store('public/app/menu');

        $gambar = $request->file('gambar');

        $extention = $gambar->getClientOriginalName();
        $filename = pathinfo($extention, PATHINFO_FILENAME);
        $extention = $gambar->getClientOriginalExtension();
        $fileNameToStore = $filename . '_' . time() . '.' . $extention;
        $path = $gambar->move(public_path() . '/assets/img/menu', $fileNameToStore);

        $do->gambar = $fileNameToStore;

        $do->save();

        return redirect('admin/menu')->with('success', 'Menu Berhasil Ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = menu::where('id',$id)->first();
        $kategori = kategori::all();

        return view('admin.menu.edit', compact('edit', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        // $rules = Validator::make($request->all(),[
        //     'id_kategori' => 'required',
        //     'nama_menu' => 'required',
        //     'harga' => 'required'
        // ]);

        // $update = menu::where('id',$id)->update([
        //     'id_kategori' => $request->id_kategori,
        //     'nama_menu' => $request->nama_menu,
        //     'harga'
        // ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = menu::where('id', $id)->delete();

        return redirect()->back()->with('error', 'Menu Berhasil Dihapus !');
    }
}
