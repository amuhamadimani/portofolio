<?php

namespace App\Http\Controllers;

use App\Models\order;
use App\Models\menu;
use App\Models\detail_order;
use Illuminate\Http\Request;
use Validator;
use helper;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $a = detail_order::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        $menu = menu::paginate(6);
        $pesanan = detail_order::where('status', 0)->get();

        return view('admin.order.index', compact('menu', 'id_order', 'pesanan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $rules = Validator::make($request->all(),[
            'id_menu',
            'jumlah',
            'sub_total',
        ]);
        // dd($request)

        for($i = 0; $i < count($request->jumlah); $i++){
            if($request->jumlah [$i] != null){
                detail_order::create([
                    'id_menu' => $request->id_menu[$i],
                    'jumlah' => $request->jumlah[$i],
                    'sub_total' => $request->jumlah[$i] * getMenu($request->id_menu[$i])->harga, 
                ]);
                
            }
        }
        return redirect()->back();

    }

    public function fixstore(Request $request){
        $rules = Validator::make($request->all(),[
            'id_order' => 'required',
            'no_meja' => 'required',
            'tanggal' => 'required',
            'id_user' => 'required',
            'keterangan'=> 'required'
        ]);
        
        order::create([
            'id_order' => $request->id_order,
            'no_meja' => $request->no_meja,
            'tanggal' => date('dmY'),
            'id_user' => $request->id_user,
            'keterangan' => $request->keterangan
        ]);

        detail_order::where('status', 0)->update([
            'id_order' => $request->id_order,
            'keterangan' => $request->keterangan,
            'no_meja' => $request->no_meja,
            'status' => 1,
        ]);
        
        return redirect()->back()->with('success', 'Anda Telah Memesan, Silahkan Tunggu !');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = detail_order::where('id', $id)->delete();

        return redirect()->back()->with('error', 'Pesanan Anda Telah Dibatalkan !');
    }
}
