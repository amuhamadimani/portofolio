<?php

namespace App\Http\Controllers;

use App\Models\kasir;
use App\Models\transaksi;
use App\Models\detail_order;
use App\Models\order;
use Validator;
use Illuminate\Http\Request;

class KasirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pesanan = order::where('status_order', 'belum bayar')->get();
        return view('kasir.index', compact('pesanan'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Validator::make($request->all(),[
            'id_user' => 'required',
            'id_order' => 'required',
            'total_bayar' => 'required'
        ]);

        transaksi::create([
            'id_user' => $request->id_user,
            'id_order' => $request->id_order,
            'tanggal' => date('Y-m-d'),
            'total_bayar' => $request->total
        ]);
        
        $id_order = $request->id_order;

        order::where('id_order', $id_order)->update([
            'status_order' => 'dibayar',
        ]);

        return redirect('kasir/')->with('success', 'Transaksi Berhasil !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\kasir  $kasir
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = detail_order::where('id_order', $id)->get();
        $sum = detail_order::where('id_order', $id)->sum('sub_total');
        $pesanan = detail_order::where('id_order', $id)->first();

        return view('kasir.show', compact('detail','pesanan', 'sum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\kasir  $kasir
     * @return \Illuminate\Http\Response
     */
    public function edit(kasir $kasir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\kasir  $kasir
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, kasir $kasir)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\kasir  $kasir
     * @return \Illuminate\Http\Response
     */
    public function destroy(kasir $kasir)
    {
        //
    }
}
