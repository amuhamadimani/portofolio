<?php

namespace App\Http\Controllers;

use App\Models\waiter;
use App\Models\menu;
use App\Models\order;
use App\Models\transaksi;
use App\Models\detail_order;
use Illuminate\Http\Request;
use Validator;
use helper;

class WaiterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $a = detail_order::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        $menu = menu::paginate(6);
        $pesanan = detail_order::where('status',0)->get();

        return view('waiter.index', compact('menu', 'id_order', 'pesanan'));
    }
    public function indexReport()
    {
        $report = transaksi::all();

        return view('waiter.report.index', compact('report'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $id)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Validator::make($request->all(),[
            'id_menu',
            'jumlah',
            'sub_total',
        ]);
        // dd($request)

        for($i = 0; $i < count($request->jumlah); $i++){
            if($request->jumlah [$i] != null){
                detail_order::create([
                    'id_menu' => $request->id_menu[$i],
                    'jumlah' => $request->jumlah[$i],
                    'sub_total' => $request->jumlah[$i] * getMenu($request->id_menu[$i])->harga, 
                ]);
            }
        }
        return redirect()->back();
    }
    public function fixstore(Request $request){
        $rules = Validator::make($request->all(),[
            'id_order' => 'required',
            'no_meja' => 'required',
            'tanggal' => 'required',
            'id_user' => 'required',
            'keterangan'=> 'required'
        ]);
        
        order::create([
            'id_order' => $request->id_order,
            'no_meja' => $request->no_meja,
            'tanggal' => date('dmY'),
            'id_user' => $request->id_user,
            'keterangan' => $request->keterangan
        ]);

        detail_order::where('status', 0)->update([
            'id_order' => $request->id_order,
            'keterangan' => $request->keterangan,
            'status' => 1,
        ]);
        
        return redirect()->back();
        
    }
    public function excel(){
        $excel = transaksi::all();
        return view('waiter.report.excel', compact('excel'));

    }
    public function exceltanggal($t1, $t2){
        $takhir = transaksi::whereBetween('tanggal',[$t1,$t2])->get();
            $sum = transaksi::whereBetween('tanggal',[$t1,$t2])->sum('total_bayar');
            $tawal = $t1;
            $taak = $t2;
        
            return view('waiter.report.exceltanggal', ['data'=>$takhir,'total'=>$sum, 'tawal'=>$tawal, 'taak'=>$taak]);
    }

    public function pdf(){
        $pdf = transaksi::all();
        return view('waiter.report.pdf', compact('pdf'));
    }
    
    public function pdftanggal($t1, $t2){
            $takhir = transaksi::whereBetween('tanggal',[$t1,$t2])->get();
            $sum = transaksi::whereBetween('tanggal',[$t1,$t2])->sum('total_bayar');
            $tawal = $t1;
            $taak = $t2;
        
            return view('waiter.report.pdftanggal', ['data'=>$takhir,'total'=>$sum, 'tawal'=>$tawal, 'taak'=>$taak]);
    }

    public function tanggal(Request $request)
    {
            $takhir = transaksi::whereBetween('tanggal',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            $sum = transaksi::whereBetween('tanggal',[$request->tanggal_awal, $request->tanggal_akhir])->sum('total_bayar');
            

            Session(['t1'=>$request->tanggal_awal]);
            Session(['t2'=>$request->tanggal_akhir]);
            

            return view('waiter.report.tanggal', ['data'=>$takhir, 't1'=>$request->tanggal_awal, 't2'=>$request->tanggal_akhir,'t3'=>$request->ontanggal, 'total'=>$sum]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\waiter  $waiter
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $pesanan = detail_order::where('status', 1)->get();
        $order = order::where('status_order','belum bayar')->get();

        return view('waiter.show',compact('pesanan','order'));
    }
    public function showdetail($id)
    {
        $pesanan = detail_order::where('id_order', $id)->get();
        $order = order::where('status_order','belum bayar')->get();
        $id_order = order::where('status_order','belum bayar')->first();

        return view('waiter.showdetail',compact('pesanan','order','id_order'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\waiter  $waiter
     * @return \Illuminate\Http\Response
     */
    public function edit(waiter $waiter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\waiter  $waiter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        detail_order::where('id_order', $id)->update([
            'status' => 2,
        ]);
        
        return redirect('waiter/show')->with('success','Pesanan Diantar !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\waiter  $waiter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = detail_order::where('id', $id)->delete();

        return redirect()->back();
    }
}
