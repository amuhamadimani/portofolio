<?php

namespace App\Http\Controllers;

use App\Models\kategori;
use Illuminate\Http\Request;
use Validator;

use App\Exports\KategoriExport;
use App\Imports\KategoriImport;
use Maatwebsite\Excel\Facades\Excel;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = kategori::all();

        return view('admin.kategori.create', compact('kategori'));
    }

    public function export()
    {
        return Excel::download(new KategoriExport(), 'kategori.xlsx');
    }

    public function import(Request $request)
    {
        $kategori = Excel::toCollection(new KategoriImport(), $request->file('import_file'));
        foreach($kategori[0] as $val){
            Kategori::where('id', $val[0])->update([
                'kategori' => $val[1]
            ]);
        }
        return redirect()->route('kategori.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $rules = Validator::make($request->all(),[
                'kategori' => 'required'
            ]);

            $kategori = kategori::create([
                'kategori' => $request->kategori,
            ]);

            return redirect()->back()->with('success', 'Kategori Berhasil Ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(kategori $kategori)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = kategori::all();
        $edit = kategori::where('id', $id)->first();

        return view('admin.kategori.edit', compact('kategori', 'edit'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $rules = Validator::make($request->all(),[
            'kategori' => 'required'
        ]);
        $update = kategori::where('id', $id)->update([
            'kategori' => $request->kategori
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dstroy = kategori::where('id', $id)->delete();

        return redirect()->back()->with('error', 'Kategori Berhasil Dihapus !');
    }
}
