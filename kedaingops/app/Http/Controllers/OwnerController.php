<?php

namespace App\Http\Controllers;

use App\Models\owner;
use App\Models\transaksi;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $report = transaksi::all();

        return view('owner.report.index', compact('report'));
    }
    public function excel(){
        $excel = transaksi::all();
        return view('owner.report.excel', compact('excel'));

    }
    public function exceltanggal($t1, $t2){
        $takhir = transaksi::whereBetween('tanggal',[$t1,$t2])->get();
            $sum = transaksi::whereBetween('tanggal',[$t1,$t2])->sum('total_bayar');
            $tawal = $t1;
            $taak = $t2;
        
            return view('owner.report.exceltanggal', ['data'=>$takhir,'total'=>$sum, 'tawal'=>$tawal, 'taak'=>$taak]);
    }

    public function pdf(){
        $pdf = transaksi::all();
        return view('owner.report.pdf', compact('pdf'));
    }
    
    public function pdftanggal($t1, $t2){
            $takhir = transaksi::whereBetween('tanggal',[$t1,$t2])->get();
            $sum = transaksi::whereBetween('tanggal',[$t1,$t2])->sum('total_bayar');
            $tawal = $t1;
            $taak = $t2;
        
            return view('owner.report.pdftanggal', ['data'=>$takhir,'total'=>$sum, 'tawal'=>$tawal, 'taak'=>$taak]);
    }

    public function tanggal(Request $request)
    {
            $takhir = transaksi::whereBetween('tanggal',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            $sum = transaksi::whereBetween('tanggal',[$request->tanggal_awal, $request->tanggal_akhir])->sum('total_bayar');
            

            Session(['t1'=>$request->tanggal_awal]);
            Session(['t2'=>$request->tanggal_akhir]);
            

            return view('owner.report.tanggal', ['data'=>$takhir, 't1'=>$request->tanggal_awal, 't2'=>$request->tanggal_akhir,'t3'=>$request->ontanggal, 'total'=>$sum]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function show(owner $owner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function edit(owner $owner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, owner $owner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\owner  $owner
     * @return \Illuminate\Http\Response
     */
    public function destroy(owner $owner)
    {
        //
    }
}
