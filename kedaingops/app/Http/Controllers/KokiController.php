<?php

namespace App\Http\Controllers;

use App\Models\koki;
use App\Models\detail_order;
use App\Models\order;
use Illuminate\Http\Request;

class KokiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $pesanan = detail_order::where('status_detail_order', 'pending')->get();
        $order = order::orderBy('status_order', 'DESC')->where('status_order', 'belum bayar')->get();
        
        return view('koki.index', compact('pesanan','order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\koki  $koki
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = detail_order::where('id_order', $id)->get();
        $pesanan = order::where('id_order', $id)->first();
        
        return view('koki.show', compact('detail','pesanan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\koki  $koki
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\koki  $koki
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);
        detail_order::where('id_order',$id)->update([
            'status_detail_order' => 'selesai dimasak',
        ]);

        return redirect()->back()->with('success', 'Pesanan Selesai Dimasak, Siap Diantar !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\koki  $koki
     * @return \Illuminate\Http\Response
     */
    public function destroy(koki $koki)
    {
        //
    }
}
