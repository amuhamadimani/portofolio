 <?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  
Route::get('/', function () {
    return view('welcome');
});
 

Route::group(['prefix'=>'admin', 'middleware' => 'admin'],function(){
	Route::get('/', 'AdminController@index');

	Route::group(['prefix'=>'roomclass'],function(){
		Route::get('/', ['as' =>'roomclass.index', 'uses'=>'RoomclassController@index']);
		Route::get('/create', ['as' =>'roomclass.create', 'uses'=>'RoomclassController@create']);
		Route::post('/store', ['as' =>'roomclass.store', 'uses'=>'RoomclassController@store']);
		Route::get('/edit', ['as' =>'roomclass.edit', 'uses'=>'RoomclassController@edit']);
		Route::patch('/update', ['as' =>'roomclass.update', 'uses'=>'RoomclassController@update']);
		Route::get('/delete', ['as' =>'roomclass.delete', 'uses'=>'RoomclassController@destroy']);
	});

	Route::group(['prefix'=>'fasilitas'], function(){
		Route::get('/', ['as' =>'fasilitas.index', 'uses'=>'FasilitasController@index']);
		Route::post('/store', ['as' =>'fasilitas.store', 'uses'=>'FasilitasController@store']);
		Route::get('/edit/{id}', ['as' =>'fasilitas.edit', 'uses'=>'FasilitasController@edit']);
		Route::post('/update/{id}', ['as' =>'fasilitas.update', 'uses'=>'FasilitasController@update']);
		Route::get('/delete/{id}', ['as' =>'fasilitas.delete', 'uses'=>'FasilitasController@destroy']);

	});

	Route::group(['prefix'=>'kamar'],function(){
		Route::get('/', ['as'=>'kamar.index', 'uses'=>'KamarController@index']);
		Route::get('/create', ['as'=>'kamar.create', 'uses'=>'KamarController@create']);
		Route::post('/store', ['as'=>'kamar.store', 'uses'=>'KamarController@store']);
		Route::get('/edit/{id}', ['as'=>'kamar.edit', 'uses'=>'KamarController@edit']);
		Route::post('/update/{id}', ['as'=>'kamar.update', 'uses'=>'KamarController@update']);
		Route::get('/delete', ['as'=>'kamar.delete', 'uses'=>'KamarController@destroy']);
	});

	Route::group(['prefix'=>'order'],function(){
		Route::get('/', ['as'=>'order.index', 'uses'=>'OrderController@index']);
		Route::get('/create', ['as'=>'order.create', 'uses'=>'OrderController@create']);
		Route::post('/store', ['as'=>'order.store', 'uses'=>'OrderController@store']);
		Route::get('/edit', ['as'=>'order.edit', 'uses'=>'OrderController@edit']);
		Route::patch('/update', ['as'=>'order.update', 'uses'=>'OrderController@update']);
		Route::get('/delete', ['as'=>'order.delete', 'uses'=>'OrderController@destroy']);

		Route::group(['prefix'=>'transaksi'],function(){
			Route::get('/{id}', ['as'=>'transaksi.index', 'uses'=>'TransaksiController@index']);
			Route::get('/create', ['as'=>'transaksi.create', 'uses'=>'TransaksiController@create']);
			Route::post('/store/{id}', ['as'=>'transaksi.store', 'uses'=>'TransaksiController@store']);
			Route::get('/edit', ['as'=>'transaksi.edit', 'uses'=>'TransaksiController@edit']);
			Route::patch('/update', ['as'=>'transaksi.update', 'uses'=>'TransaksiController@update']);
			Route::get('/delete', ['as'=>'transaksi.delete', 'uses'=>'TransaksiController@destroy']);
		});
	});
	Route::group(['prefix'=>'transaksi'],function(){
			Route::get('/{id}', ['as'=>'transaksi.index', 'uses'=>'TransaksiController@index']);
			Route::get('/create', ['as'=>'transaksi.create', 'uses'=>'TransaksiController@create']);
			Route::post('/store/{id}', ['as'=>'transaksi.store', 'uses'=>'TransaksiController@store']);
			Route::get('/edit', ['as'=>'transaksi.edit', 'uses'=>'TransaksiController@edit']);
			Route::patch('/update', ['as'=>'transaksi.update', 'uses'=>'TransaksiController@update']);
			Route::get('/delete', ['as'=>'transaksi.delete', 'uses'=>'TransaksiController@destroy']);
		});

		Route::group(['prefix'=>'detailroom'],function(){
			Route::get('/', ['as'=>'detailroom.index', 'uses'=>'DetailRoomController@index']);
			Route::get('/transaksi', ['as'=>'detailroom.transaksi', 'uses'=>'DetailRoomController@transaksi']);
			Route::get('/create', ['as'=>'detailroom.create', 'uses'=>'DetailRoomController@create']);
			Route::post('/store/{id}', ['as'=>'detailroom.store', 'uses'=>'DetailRoomController@store']);
			Route::get('/edit', ['as'=>'detailroom.edit', 'uses'=>'DetailRoomController@edit']);
			Route::patch('/update', ['as'=>'detailroom.update', 'uses'=>'DetailRoomController@update']);
			Route::get('/delete', ['as'=>'detailroom.delete', 'uses'=>'DetailRoomController@destroy']);
		});
		Route::group(['prefix'=>'report'],function(){
			Route::get('/', ['as'=>'report.index', 'uses'=>'ReportController@index']);
			Route::get('/transaksi', ['as'=>'report.transaksi', 'uses'=>'ReportController@transaksi']);
			Route::get('/kamar', ['as'=>'report.kamar', 'uses'=>'ReportController@kamar']);
			Route::get('/income', ['as'=>'report.income', 'uses'=>'ReportController@income']);
			Route::post('/kamar_tanggal', ['as'=>'report.kamar_tanggal', 'uses'=>'ReportController@kamar_tanggal']);
			Route::post('/transaksi_tanggal', ['as'=>'report.transaksi_tanggal', 'uses'=>'ReportController@transaksi_tanggal']);
		});
});


Route::group(['prefix'=>'manager', 'middleware' => 'manager'],function(){
	Route::get('/', 'ManagerController@index');

	Route::group(['prefix'=>'report'],function(){
		Route::get('/', ['as'=>'report.index', 'uses'=>'ReportController@manager_index']);
		Route::get('/transaksi', ['as'=>'report.transaksi', 'uses'=>'ReportController@manager_transaksi']);
		Route::get('/kamar', ['as'=>'report.kamar', 'uses'=>'ReportController@manager_kamar']);
		Route::get('/income', ['as'=>'report.income', 'uses'=>'ReportController@income']);
		Route::post('/kamar_tanggal', ['as'=>'report.kamar_tanggal', 'uses'=>'ReportController@kamar_tanggal']);
		Route::post('/kamar_tanggal', ['as'=>'report.kamar_tanggal', 'uses'=>'ReportController@manager_kamar_tanggal']);
		Route::post('/transaksi_tanggal', ['as'=>'report.transaksi_tanggal', 'uses'=>'ReportController@transaksi_tanggal']);
		Route::post('/transaksi_tanggal', ['as'=>'report.transaksi_tanggal', 'uses'=>'ReportController@manager_transaksi_tanggal']);
	});
});
Route::group(['prefix'=>'user', 'middleware' => 'user'],function(){
	Route::get('/', 'PelangganController@index');

	
	Route::group(['prefix'=>'order'],function(){
		Route::get('/', ['as'=>'user.order.index', 'uses'=>'OrderController@order_index']);
		Route::get('/create', ['as'=>'user.order.create', 'uses'=>'OrderController@create']);
		Route::post('/store', ['as'=>'user.order.store', 'uses'=>'OrderController@user_store']);
		Route::get('/edit', ['as'=>'user.order.edit', 'uses'=>'OrderController@edit']);
		Route::patch('/update', ['as'=>'user.order.update', 'uses'=>'OrderController@update']);
		Route::get('/delete', ['as'=>'user.order.delete', 'uses'=>'OrderController@destroy']);
	});

	Route::group(['prefix'=>'kamar'],function(){
		Route::get('/', ['as'=>'user.kamar.index', 'uses'=>'KamarController@kamar_index']);
		Route::get('/create', ['as'=>'user.kamar.create', 'uses'=>'KamarController@create']);
		Route::post('/store', ['as'=>'user.kamar.store', 'uses'=>'KamarController@store']);
		Route::get('/edit', ['as'=>'user.edit', 'uses'=>'KamarController@edit']);
		Route::patch('/update', ['as'=>'user.kamar.update', 'uses'=>'KamarController@update']);
		Route::get('/delete', ['as'=>'user.delete', 'uses'=>'KamarController@destroy']);
	});
});



Route::post('/ajax/get-kamar', 'AjaxController@getKamar')->name('getKamar');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
