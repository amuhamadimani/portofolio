@extends('templates.stisla')

@section('title','Hotel-Wk')

@section('header')
	<div class="section-header">
	    <div class="col-md-3">
	      <h1>Admin Dashboard</h1>
	    </div>
	    <div class="col-md-1">
	      <h1>|</h1>
	    </div>
	    <div class="col-md-2">
	      <a href="{{ route('kamar.index') }}" class="btn btn-lg btn-icon icon-left btn-warning">
	        <i class="fas fa-bed"></i>
	        <label for="">Data Kamar</label>
	      </a>
	    </div>
	    <div class="col-md-2">
	      <a href="{{ route('order.index') }}" class="btn btn-lg btn-icon icon-left btn-info">
	        <i class="fas fa-play"></i>
	        <label for="">Pesan Kamar</label>
	      </a>
	    </div>
	    <div class="col-md-2">
	      <a href="{{ route('detailroom.index') }}" class="btn btn-lg btn-icon icon-left btn-primary">
	        <i class="fas fa-power-off"></i>
	        <label for="">Checkout</label>
	      </a>
	    </div>
	    <div class="col-md-2">
	      <a href="" class="btn btn-lg btn-icon icon-left btn-warning">
	        <i class="fas fa-bed"></i>
	      </a>
	    </div>
	</div>
@endsection

<style>
.kotak{
	width: 100%;
	height: 40px;
	margin-left: 60px;
	background-color: #C1BDBD;
	border-radius: 10px;
}
.carousel-inner img {
    width: 100%;
    height: 100%;
  }
</style>

@section('content')
	<div class="container-fluid">
	  <div class="row"  style="height: 76%">
	    <div class="col-md-6">
	      <div class="row">
	        <div class="col-md-6">
	          <div class="card card-statistic-1">
	            <div class="card-icon bg-primary">
	              <i class="fas fa-users" style="padding-top: 25px;"></i>
	            </div>
	            <div class="card-wrap">
	              <div class="card-header">
	                <h4>Tamu Hari Ini</h4>
	              </div>
	              <div class="card-body">
	                {{ $today->count() }}
	              </div>
	            </div>
	          </div>
	        </div>
	        <div class="col-md-6">
	          <div class="card card-statistic-1">
	            <div class="card-icon bg-warning">
	              <i class="fas fa-bed" style="padding-top: 25px;"></i>
	            </div>
	            <div class="card-wrap">
	              <div class="card-header">
	                <h4>Total Kamar</h4>
	              </div>
	              <div class="card-body">
	                {{ $kamar }}
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	      <div class="row">
	        <div class="col-md-6">
	          <div class="card card-statistic-1">
	            <div class="card-icon bg-info">
	              <i class="fas fa-user-alt" style="padding-top: 25px;"></i>
	            </div>
	            <div class="card-wrap">
	              <div class="card-header">
	                <h4>Total User</h4>
	              </div>
	              <div class="card-body">
	                 {{ $user }}
	              </div>
	            </div>
	          </div>
	        </div>
	         <div class="col-md-6">
	          <div class="card card-statistic-1">
	            <div class="card-icon bg-success">
	              <i class="fas fa-dollar-sign" style="padding-top: 25px;"></i>
	            </div>
	            <div class="card-wrap">
	              <div class="card-header">
	                <h4>Total Pendapatan</h4>
	              </div>
	              <div class="card-body" style="font-size: 15px;">
	        		Rp. {{ number_format($income,0,0,'.') }},00
	              </div>
	            </div>
	          </div>
	        </div>
	        <div class="col-md-6">
				<h3 class="card-title">Report</h3>
				<a href="">
		          <div class="card card-statistic-1">
		            <div class="card-icon bg-info">
		              <i class="fas fa-list" style="padding-top: 25px;"></i>
		            </div>
		            <div class="card-wrap">
		              <div class="card-header">
		              </div>
		              <div class="card-body">
		                <h5>Report Transaksi</h5>
		              </div>
		            </div>
		          </div>	
				</a>
	        </div>
	        <div class="col-md-6">
				<h3 class="card-title"><br></h3>
				<a href="{{ url('admin/kamar') }}">
		          <div class="card card-statistic-1">
		            <div class="card-icon bg-warning">
		              <i class="fas fa-bed" style="padding-top: 25px;"></i>
		            </div>
		            <div class="card-wrap">
		              <div class="card-header">
		              </div>
		              <div class="card-body">
		                <h5>Report Kamar</h5>
		              </div>
		            </div>
		          </div>	
				</a>
	        </div>
	      </div>
	    </div>
	    <div class="col-md-6">
	       <div id="demo" class="carousel slide" data-ride="carousel">
	          <ul class="carousel-indicators">
	            <li data-target="#demo" data-slide-to="0" class="active"></li>
	            <li data-target="#demo" data-slide-to="1"></li>
	            <li data-target="#demo" data-slide-to="2"></li>
	          </ul>
	          <div class="carousel-inner" style="height: 70%">
	            <div class="carousel-item active">
	              <img src="{{ asset('dist/img/hotel.jpg') }}" alt="Los Angeles" width="1100" height="500">
	              <div class="carousel-caption">
	                <h3>Ngendongin</h3>
	                <p>Butuh Kamar untuk menginap ? Ngendongin aja</p>
	              </div>   
	            </div>
	            <div class="carousel-item">
	              <img src="{{ asset('dist/img/hotel.jpg') }}" alt="Chicago" width="1100" height="500">
	              <div class="carousel-caption">
	                <h3>Hotel Bintang Lima Harga Merakyat</h3>
	                <p>Di Ngendongin , tidak perlu keluar uang banyak</p>
	              </div>   
	            </div>
	            <div class="carousel-item">
	              <img src="{{ asset('dist/img/hotel.jpg') }}" alt="New York" width="1100" height="500">
	              <div class="carousel-caption">
	                <h3>New York</h3>
	                <p>We love the Big Apple!</p>
	              </div>   
	            </div>
	          </div>
	          <a class="carousel-control-prev" href="#demo" data-slide="prev">
	            <span class="carousel-control-prev-icon"></span>
	          </a>
	          <a class="carousel-control-next" href="#demo" data-slide="next">
	            <span class="carousel-control-next-icon"></span>
	          </a>
	        </div>
	    </div>
	  </div>
	</div>
</div>
@endsection