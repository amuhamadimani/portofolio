<div class="row" style="margin-top:10px" id="input-{{ $key }}">
    <div class="col-md-6">
        <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Kamar</label>
            <div class="col-sm-12 col-md-7">
                 <input type="number" name="no_kamar[{{ $key }}]" class="form-control" required="" autocomplete="off">
            </div>
        </div>
        <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Bed</label>
            <div class="col-sm-12 col-md-7">
                 <select name="bed[{{ $key }}]" class="form-control">
                     <option value="Singel Bed">Singel Bed</option>
                     <option value="Double Bed">Double Bed</option>
                </select>
            </div>
        </div>
        <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Room Class</label>
            <div class="col-sm-12 col-md-7">
                <div class="form-group">
                    <div class="input-group">
                        <select name="roomclass[{{ $key }}]" class="form-control">
                             @foreach($room as $val)
                                <option value="{{ getRoom($val->id_roomclass) }}">{{ getRoom($val->id_roomclass) }}</option>
                             @endforeach
                        </select>
                        <div class="input-group-append">
                          <a href="{{ Route('roomclass.index') }}" class="btn btn-primary"><i class="fas fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                 
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Harga</label>
            <div class="col-sm-12 col-md-7">
                 <input type="number" name="harga[{{ $key }}]" class="form-control" required="" autocomplete="off">
            </div>
        </div>
        <div class="form-group row mb-4">
            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
            <div class="col-sm-12 col-md-7">
                 <input type="file" name="gambar[{{ $key }}]" class="form-control" required="" autocomplete="off">
            </div>
        </div>
        <button class="btn btn-danger float-right" name="add_kamar" type="submit" onclick="$(this).removeItem('#input-{{ $key }}')">x</button>
    </div>
</div>