@extends('templates.stisla')

@section('title', 'Hotel-WK')

@section('header')
	<div class="section-header">
		<h1>Data Kamar</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item">Kamar</div>
		</div>
	</div>
@endsection

@section('content')

	<div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
          	<a href="{{ url('admin/kamar/create') }}" class="btn btn-primary float-right"><i class="fas fa-plus mr-2"></i><span>Tambah Kamar</span></a><br><br><br>
            <div class="table-responsive">
              <table class="table table-striped" id="table-1">
                <thead>                                 
                  <tr>
                    <th class="text-center">
                      No
                    </th>
                    <th>No Kamar</th>
                    <th>Bed</th>
                    <th>Roomclass</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody> 
                @foreach($data as $val)                                
                  <tr>
                    <td>{{ $loop->index +1 }}</td>
                    <td>{{ $val->no_kamar }}</td>
                    <td>{{ $val->bed }}</td>
                    <td>{{ $val->roomclass }}</td>
                    <td>Rp. {{ number_format($val->harga,0,0,'.') }},00 / Malam</td>
                    <td><img src="{{ asset('assets/img/kamar/'.$val->gambar) }}" width="150" height="100" alt=""></td>
                    <td>
                      <a href="{{ url('admin/kamar/edit/'.$val->id) }}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                      <a href="{{ url('admin/kamar/delete/'.$val->id) }}" class="btn btn-danger" data-confirm="Realy?|Do you want to continue?" data-confirm-yes="alert('Deleted :)');"><i class="fas fa-trash"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection


