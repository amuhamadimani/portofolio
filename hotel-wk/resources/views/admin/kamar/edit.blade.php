@extends('templates.stisla')

@section('title', 'Hotel-WK')

@section('header')
	<div class="section-header">
		<h1>Kamar</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ url('admin/kamar') }}">Kamar</a>
			</div>
			<div class="breadcrumb-item">Create</div>
		</div>
	</div>
@endsection
 
@section('content')
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<form action="{{ url('/admin/kamar/update/'.$edit->id) }}" method="post" enctype="multipart/form-data">
					@csrf
						<input type="hidden" name="id_roomclass" value="" class="form-control" required="" autocomplete="off">
						
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Kamar</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="number" value="{{ $edit->no_kamar }}" name="no_kamar" class="form-control" required="" autocomplete="off">
				            </div>
						</div>
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Bed</label>
							<div class="col-sm-12 col-md-7">
				             	<select name="bed" class="form-control">
				             		<option value="{{ $edit->bed }}">{{ $edit->bed }}</option>
				             		<option value="Singel Bed">Singel Bed</option>
				             		<option value="Double Bed">Double Bed</option>
			                    </select>
				            </div>
						</div>
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Room Class</label>
							<div class="col-sm-12 col-md-7">
								<div class="form-group">
									<div class="input-group">
										<select name="roomclass" class="form-control">
											<option value="{{ $edit->roomclass }}">{{ $edit->roomclass }}</option>
						             		@foreach($room as $val)
												<option value="{{ getRoom($val->id_roomclass) }}">{{ getRoom($val->id_roomclass) }}</option>
						             		@endforeach
					                    </select>
					                    <div class="input-group-append">
					                      <a href="{{ Route('roomclass.index') }}" class="btn btn-primary"><i class="fas fa-plus"></i></a>
					                    </div>
									</div>
								</div>
				             	
				            </div>
						</div>

						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Harga</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="number" value="{{ $edit->harga }}" name="harga" class="form-control" required="" autocomplete="off">
				            </div>
						</div>
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="file" name="gambar" class="form-control" required="" autocomplete="off">
				            </div>
						</div>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-sm-12 col-md-7">
				              <button class="btn btn-primary" type="submit">Update</button>
				              <a href="{{ route('kamar.index') }}" class="btn btn-warning ml-3">Kembali</a>
				            </div>
						</div>

					</form>	
				</div>
			</div>
		</div>
	</div>

@endsection