@extends('templates.stisla')

@section('title', 'Hotel-WK')

@section('header')
	<div class="section-header">
		<h1>Kamar</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ url('admin/kamar') }}">Kamar</a>
			</div>
			<div class="breadcrumb-item">Create</div>
		</div>
	</div>
@endsection
 
@section('content')
<form action="" method="post" enctype="multipart/form-data"></form>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<form action="{{ url('/admin/kamar/store') }}" method="post" enctype="multipart/form-data">

					@csrf
						<input type="hidden" name="id_roomclass" value="" class="form-control" required="" autocomplete="off">
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Kamar</label>
									<div class="col-sm-12 col-md-7">
										 <input type="number" name="no_kamar" class="form-control" required="" autocomplete="off">
									</div>
								</div>
								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Bed</label>
									<div class="col-sm-12 col-md-7">
										 <select name="bed" class="form-control">
											 <option value="Singel Bed">Singel Bed</option>
											 <option value="Double Bed">Double Bed</option>
										</select>
									</div>
								</div>
								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Room Class</label>
									<div class="col-sm-12 col-md-7">
										<div class="form-group">
											<div class="input-group">
												<select name="roomclass" class="form-control">
													 @foreach($room as $val)
														<option value="{{ getRoom($val->id_roomclass) }}">{{ getRoom($val->id_roomclass) }}</option>
													 @endforeach
												</select>
												<div class="input-group-append">
												  <a href="{{ Route('roomclass.index') }}" class="btn btn-primary"><i class="fas fa-plus"></i></a>
												</div>
											</div>
										</div>
										 
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Harga</label>
									<div class="col-sm-12 col-md-7">
										 <input type="number" name="harga" class="form-control" required="" autocomplete="off">
									</div>
								</div>
								<div class="form-group row mb-4">
									<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
									<div class="col-sm-12 col-md-7">
										 <input type="file" name="gambar" class="form-control" required="" autocomplete="off">
									</div>
								</div>
								<button class="btn btn-primary float-right" id="add-kamar" type="button"><i class="fas fa-plus"></i> More</button>
								
							</div>
							<div class="col-md-12">
								<div id="input-list"> </div>

								{{-- append ajax --}}
									<input type="hidden" id="key" value="0">
								<br>
							</div>
							
						</div>

						<div class="row">
							<div class="col-md-10"></div>
							<div class="col-sm-12 col-md-2">
				              <button class="btn btn-success" type="submit">Simpan</button>
				              <a href="{{ route('kamar.index') }}" class="btn btn-warning ml-3">Kembali</a>
				            </div>
						</div>

					</form>	
				</div>
			</div>
		</div>
	</div>

@endsection

@section('script')
  
    <script>
        $(document).ready(function(){

            var key_input = $("#key").val();

            $("#add-kamar").on("click", function(e){ 

                e.preventDefault();

                key_input++;

                var url = '/ajax/get-kamar'

                var data = {
                    key:key_input
                }
                
                $.ajax({
                    url:url,
                    data:data,
                    type:"POST",
                    success:function(result){
                        $("#input-list").append(result.html);
                        $("#key").val(result.key);
                    }
                })
            
            });
        });

        $.fn.removeItem = function(id){
            $(id).remove();
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
    </script>

@endsection