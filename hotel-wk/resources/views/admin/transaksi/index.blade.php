@extends('templates.stisla')

@section('title', 'Room Class')

@section('header')
	<div class="section-header">
		<h1>Transaksi</h1>
		<div class="section-header-breadcrumb">
      <div class="breadcrumb-item active">
        <a href="{{ url('admin') }}">Dashboard</a>
      </div>
			<div class="breadcrumb-item active">
				<a href="{{ route('order.index') }}">Pesan Kamar</a>
			</div>
			<div class="breadcrumb-item">Transaksi</div>
			{{-- <div class="breadcrumb-item"></div> --}}
		</div>
	</div>
@endsection

@section('content')
<form action="{{ url('admin/order/transaksi/store/'.$order->id_order) }}" method="post">
  @csrf
	<div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">
              Pembayaran | User : {{ Auth::user()->name }}
            </h5>
          </div>
          <div class="card-body">

                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-4">
                    <div class="form-group row ">
                      <label class="col-form-label text-md-right col-12 col-md-3 ">Durasi</label>
                      <div class="">
                        <input type="text" name="total_harga" value="{{ $order->durasi }} Hari" class="form-control text-center" readonly="" required="" autocomplete="off">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group row ">
                      <label class="col-form-label text-md-right col-12 col-md-3 ">Harga</label>
                      <div class="">
                        <input type="text" name="total_harga" value="Rp. {{ number_format(getHarga($order->no_kamar,0,0,'.')) }} / Hari" class="form-control text-center" readonly="" required="" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row ">
                  <label class="col-form-label text-md-right col-12 col-md-3 ">Total Harga</label>
                  <div class="">
                    <input type="text" name="total_harga" id="total" value="{{ $order->total_harga }}" class="form-control text-center" readonly="" required="" autocomplete="off">
                  </div>
                </div>
                <div class="form-group row ">
                  <label class="col-form-label text-md-right col-12 col-md-3 ">Bayar</label>
                  <div class="">
                    <input type="number" name="total_harga" id="bayar" value="" class="form-control text-center"  required="" autocomplete="off">
                  </div>
                </div>
                <div class="form-group row ">
                  <label class="col-form-label text-md-right col-12 col-md-3 ">Kembalian</label>
                  <div class="">
                    <input type="text" name="total_harga" id="kembalian" value="" class="form-control text-center" readonly="" required="" autocomplete="off">
                  </div>
                </div>
           <button id="btnBayar" class="btn btn-success btn-lg btn-block">Bayar</button>

          </div>
        </div>
      </div>
  </div>
</form>
@endsection
