@extends('templates.stisla')

@section('title', 'Hotel-WK')
 
@section('header')
	<div class="section-header">
		<h1>Fasilitas</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('kamar.index') }}">Kamar</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('kamar.create') }}">Kamar Create</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('roomclass.index') }}">Room Class</a>
			</div>

			<div class="breadcrumb-item active">
				<a href="{{ route('roomclass.create') }}">Room Class Create</a>
			</div>
			<div class="breadcrumb-item">Fasilitas</div>
		</div>
	</div>
@endsection

@section('content')

	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<form action="{{ route('fasilitas.store') }}" method="post">
						@csrf

						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Fasilitas</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="text" name="fasilitas" class="form-control" required="" autocomplete="off" >
				            </div> 
						</div>
						<div class="form-group row mb-4">
				            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
				            <div class="col-sm-12 col-md-7">
				              <button class="btn btn-primary" type="submit">Tambah</button>
				              <a href="{{ route('roomclass.create') }}" class="btn btn-warning ml-3">Kembali</a>
				            </div>
				        </div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<table class="table table-bordered table-hover" id="dabel">
						<thead class="text-center">
							<tr>
								<th>No</th>
								<th>Fasilitas</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($fasilitas as $val)
								<tr>
									<td>{{ $loop->index + 1 }}</td>
									<td>{{ $val->fasilitas }}</td>
									<td class="text-center">
										<a href="{{ url('admin/fasilitas/edit/'.$val->id) }}" class="btn btn-info"><i class="fas fa-edit"></i></a>
										<a href="{{ url('admin/fasilitas/delete/'.$val->id) }}" class="btn btn-danger" data-confirm="Realy?|Do you want to continue?" data-confirm-yes="alert('Deleted :)');"><i class="fas fa-trash"></i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@endsection