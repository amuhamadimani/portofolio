@extends('templates.stisla')

@section('title', 'Room Class')
 
@section('header')
	<div class="section-header">
		<h1>Room Class</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ url('admin/roomclass') }}">Room Class</a>
			</div>
			<div class="breadcrumb-item">Fasilitas</div>
		</div>
	</div>
@endsection

@section('content')

	<div class="row">
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<form action="{{ url('admin/fasilitas/update/'.$edit->id) }}" method="post">
						@csrf

						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Fasilitas</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="text" name="fasilitas" value="{{ $edit->fasilitas }}" class="form-control" required="" autocomplete="off">
				            </div>
						</div>
						<div class="form-group row mb-4">
				            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
				            <div class="col-sm-12 col-md-7">
				              <button class="btn btn-success w-25" type="submit"><i class="fas fa-check"></i></button>
				            </div>
				        </div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-6">
			<div class="card">
				<div class="card-body">
					<table class="table table-bordered table-hover">
						<thead class="text-center">
							<tr>
								<th>No</th>
								<th>Fasilitas</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($fasilitas as $val)
								<tr>
									<td>{{ $loop->index + 1 }}</td>
									<td>{{ $val->fasilitas }}</td>
									<td class="text-center">
										<a href="{{ url('admin/fasilitas/edit/'.$val->id) }}" class="btn btn-info"><i class="fas fa-edit"></i></a>
										<a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@endsection