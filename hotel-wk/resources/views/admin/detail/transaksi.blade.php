@extends('templates.stisla')

@section('title', 'Room Class')

@section('header')
	<div class="section-header">
		<h1>Room Class</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item">Room Class</div>
			{{-- <div class="breadcrumb-item"></div> --}}
		</div>
	</div>
@endsection

@section('content')
	<div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-md-2">
                <a href="{{ route('kamar.index') }}" class="btn btn-info btn-lg">Detail Kamar</a>
              </div>

              <div class="col-md-4"></div>
              <div class="col-md-2">
                <a href="{{ route('detailroom.index') }}" class="btn btn-info btn-lg">Checkout</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <h4 class="card-title text-center">
              Checkout
            </h4>
            <div class="table-responsive">


              <table class="table table-striped" id="table-1">
                <thead>                                 
                  <tr>
                    <th class="text-center">
                      No
                    </th>
                    <th>Id Order</th>
                    <th>Nama</th>
                    <th>No Kamar</th>
                    <th>Checkin</th>
                    <th>Checkout</th>
                    <th>Durasi</th>
                    <th>Status Pembayaran</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody class="text-center"> 
                @foreach($order as $val)              
                  <tr>
                    <td>{{ $loop->index +1 }}</td>
                    <td>{{ $val->id_order }}</td>
                    <td>{{ $val->nama }}</td>
                    <td>{{ $val->no_kamar }}</td>
                    <td>{{ $val->checkin }}</td>
                    <td>{{ $val->checkout }}</td>
                    <td>{{ $val->durasi }}</td>
                    <td>{{ $val->status_bayar }}</td>
                    <td>
                      <a href="{{ url('admin/order/transaksi/'.$val->id_order) }}" class="btn btn-primary">Bayar</a>
                    </td>
                  </tr>    
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
