@extends('templates.stisla')

@section('title', 'Room Class')

@section('header')
  <div class="section-header">
    <h1>Room Class</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active">
        <a href="{{ url('admin') }}">Dashboard</a>
      </div>
      <div class="breadcrumb-item">Room Class</div>
      {{-- <div class="breadcrumb-item"></div> --}}
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <a href="{{ route('report.transaksi') }}" class="btn btn-lg btn-icon icon-left btn-primary">
                  <i class="fas fa-bed"></i>
                  <label for="">Report Transaksi</label>
                </a>
              </div>
              <div class="col-md-4">
                <a href="{{ route('report.kamar') }}" class="btn btn-lg btn-icon icon-left btn-info">
                  <i class="fas fa-bed"></i>
                  <label for="">Report Kamar</label>
                </a>
              </div>
              <div class="col-md-4">
                <a href="{{ route('income.index') }}" class="btn btn-lg btn-icon icon-left btn-warning">
                  <i class="fas fa-bed"></i>
                  <label for="">Report Pendapatan</label>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
