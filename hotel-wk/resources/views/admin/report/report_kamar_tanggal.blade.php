@extends('templates.stisla')

@section('title', 'Room Class')

@section('header')
  <div class="section-header">
    <h1>Room Class</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active">
        <a href="{{ url('admin') }}">Dashboard</a>
      </div>
      <div class="breadcrumb-item">Room Class</div>
      {{-- <div class="breadcrumb-item"></div> --}}
    </div>
  </div>
@endsection

@section('content')

  <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="form-group">
                    <label for="">Dari Tanggal</label>
                    <label for="">:</label>
                    <label for="">{{ @$t1 }}</label>
                
                    <label for="">-</label>
                    <label for="">{{ @$t2 }}</label>
                </div>
            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="export">
                <thead>                                 
                  <tr>
                    <th class="text-center">
                      No
                    </th>
                    <th>No Kamar</th>
                    <th>Bed</th>
                    <th>Roomclass</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                  </tr>
                </thead>
                <tbody class="text-center"> 
                @foreach($data as $val)                                
                  <tr>
                    <td>{{ $loop->index +1 }}</td>
                    <td>{{ $val->no_kamar }}</td>
                    <td>{{ $val->bed }}</td>
                    <td>{{ $val->roomclass }}</td>
                    <td>Rp. {{ number_format($val->harga,0,0,'.') }} / Malam</td>
                    <td><img src="{{ asset('assets/img/kamar/'.$val->gambar) }}" width="100" height="100" alt=""></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
