@extends('templates.stisla')

@section('title', 'Room Class')

@section('header')
	<div class="section-header">
		<h1>Room Class</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item">Room Class</div>
			{{-- <div class="breadcrumb-item"></div> --}}
		</div>
	</div>
@endsection

@section('content')
	<div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4 class="card-title text-center">
              Report Transaksi
            </h4>
          	
          </div>
          <div class="card-body">
          	<a href="{{ route('report.kamar') }}" class="btn btn-danger float-right btn-lg">Back</a>

            <form action="{{ route('report.transaksi_tanggal') }}" method="post">
            @csrf
                <div class="col-md-3">
                    <div class="form-group row">
                        <label for="">Tanggal Awal</label>
                        <input type="date" name="tanggal_awal" class="form-control">
                        <label for="">Tanggal Akhir</label>
                        <input type="date" name="tanggal_akhir" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary btn-lg" style="height:70px; margin-left: 300px; margin-top: -200px;">Lihat</button>
                </div>
            </form>
            <div class="table-responsive">
              <table class="table table-striped" id="export">
                <thead>                                 
                  <tr>
                    <th class="text-center">
                      No
                    </th>
                    <th>Id Order</th>
                    <th>Nama</th>
                    <th>No Kamar</th>
                    <th>Checkin</th>
                    <th>Checkout</th>
                    <th>Durasi</th>
                    <th>Status Pembayaran</th>
                  </tr>
                </thead>
                <tbody class="text-center"> 
                @foreach($data as $val)               
                  <tr>
                    <td>{{ $loop->index +1 }}</td>
                    <td>{{ $val->id_order }}</td>
                    <td>{{ $val->nama }}</td>
                    <td>{{ $val->no_kamar }}</td>
                    <td>{{ $val->checkin }}</td>
                    <td>{{ $val->checkout }}</td>
                    <td>{{ $val->durasi }} Malam</td>
                    <td>{{ $val->status_bayar }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
