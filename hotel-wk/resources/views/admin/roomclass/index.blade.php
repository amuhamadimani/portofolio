@extends('templates.stisla')

@section('title', 'Hotel-WK')

@section('header')
	<div class="section-header">
		<h1>Room Class</h1>
		<div class="section-header-breadcrumb">
      <div class="breadcrumb-item active">
        <a href="{{ url('admin') }}">Dashboard</a>
      </div>
      <div class="breadcrumb-item active">
        <a href="{{ route('kamar.index') }}">Kamar</a>
      </div>
			<div class="breadcrumb-item active">
        <a href="{{ route('kamar.create') }}">Kamar Create</a>
			</div>
			<div class="breadcrumb-item">Room Class</div>
			{{-- <div class="breadcrumb-item"></div> --}}
		</div>
	</div>
@endsection

@section('content')

	<div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
          	<a href="{{ url('admin/roomclass/create') }}" class="btn btn-primary float-right"><i class="fas fa-plus mr-2"></i><span>Room Class</span></a><br><br><br>
            <div class="table-responsive">
              <table class="table table-striped" id="table-1">
                <thead>                                 
                  <tr>
                    <th class="text-center">
                      No
                    </th>
                    <th>Room Class</th>
                    <th>Fasilitas</th>
                  </tr>
                </thead>
                <tbody class="text-center"> 
                @foreach($data as $val)                                
                  <tr>
                    <td>{{ $loop->index +1 }}</td>
                    <td>{{ getRoom($val->id_roomclass) }}</td>
                    <td>
                      @foreach(getFasilitas($val->id_fasilitas) as $fasilitas)
                        {{ $fasilitas->fasilitas.',' }}
                      @endforeach
                      {{-- <button data-id="{{ $val->id }}" class="btn btn-icon btn-info" data-toggle="modal" data-target="#fasilitas"><i class="fa fa-info"></i></button> --}}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
