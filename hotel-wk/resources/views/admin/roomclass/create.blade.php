@extends('templates.stisla')

@section('title', 'Room Class')

@section('header')
	<div class="section-header">
		<h1>Room Class</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('kamar.index') }}">Kamar</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('kamar.create') }}">Kamar Create</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ url('admin/roomclass') }}">Room Class</a>
			</div>
			<div class="breadcrumb-item">Create</div>
		</div>
	</div>
@endsection
 
@section('content')
<form action="" method="post" enctype="multipart/form-data"></form>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<form action="{{ url('/admin/roomclass/store') }}" method="post" enctype="multipart/form-data">
					@csrf
						<input type="hidden" name="id_roomclass" value="{{ $id_order }}" class="form-control" required="" autocomplete="off">
						
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Room Class</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="text" name="roomclass" class="form-control" required="" autocomplete="off">
				            </div>
						</div>
						<div class="form-group row mb-4">
				            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Fasilitas</label>
				            <div class="col-sm-12 col-md-7">
				               <div class="form-group">
				                  <div class="input-group">
				                    <select name="id_fasilitas[]" id="fasilitas" class="form-control" multiple="multiple">
				                      @foreach($data as $tampil)
				                        <option value="{{$tampil->id}}">{{$tampil->fasilitas}}</option>
				                      @endforeach
				                    </select>
				                    <div class="input-group-append">
				                      <a href="{{ Route('fasilitas.index') }}" class="btn btn-primary"><i class="fas fa-plus"></i></a>
				                    </div>
				                  </div>
				                </div>
				            </div>
				        </div>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-sm-12 col-md-7">
				              <button class="btn btn-primary" type="submit">Tambah</button>
				              <a href="{{ url('admin/roomclass/create')}}" class="btn btn-warning ml-3">Kembali</a>
				            </div>
						</div>

					</form>	
				</div>
			</div>
		</div>
	</div>

@endsection

@section('script')
	<script>
		$(document).ready(function(){
			$('#fasilitas').select2({
		        placeholder : 'Pilih Fasilitas',
		        tags: true
		    });
		});

	   
	</script>
@endsection