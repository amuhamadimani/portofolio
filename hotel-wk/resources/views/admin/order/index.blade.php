@extends('templates.stisla')

@section('title', 'Hotel-WK')

@section('header')
	<div class="section-header">
		<h1>Pesan Kamar</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item">Pesan Kamar</div>
			{{-- <div class="breadcrumb-item"></div> --}}
		</div>
	</div>
@endsection

@section('content')
<form action="{{ route('order.store') }}" method="post">
  @csrf
	<div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">
              Pesan Kamar | User : {{ Auth::user()->name }}
            </h5>
            <input type="hidden" name="id_order" value="{{ $id_order }}" class="form-control text-md-rigth ml-25">
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group row mb-4">
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama Pemesan</label>
                  <div class="col-md-7">
                    <input type="text" name="nama" class="form-control" required="" autocomplete="off">
                  </div>
                </div>
                <div class="form-group row mb-4">
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                  <div class="col-md-7">
                    <input type="email" name="email" class="form-control" required="" autocomplete="off">
                  </div>
                </div>
                <div class="form-group row mb-4">
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pilih Kamar No.</label>
                  <div class="col-md-7">
                      <select name="no_kamar" class="form-control" required="" autocomplete="off" id="kamar">
                        @foreach($kamar as $val)
                          <option value="{{ $val->no_kamar }}">{{ $val->no_kamar }}</option>
                        @endforeach
                      </select>
                      <br>
                      <div>
                      </div>
                  </div>
                </div>
                <div class="form-group row mb-4"> </div>
                <div class="form-group row mb-4">
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Check In</label>
                  <div class="col-md-7">
                    <input type="date" name="checkin" class="form-control" required="" autocomplete="off">
                  <br>
                  </div>
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Check Out</label>
                  <div class="col-md-7">
                    <input type="date" name="checkout" class="form-control" required="" autocomplete="off">
                  </div>
                </div>
                <div class="form-group row mb-4">
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Booking ?</label>
                  <div class="col-md-7">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                      <p><input type="radio" name="booking" value="1"> Ya</p>
                    </div>
                    <div class="col-md-4">
                      <p><input type="radio" name="booking" value="2"> Tidak</p>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                  <button class="btn btn-success btn-lg btn-block" >Pesan Sekarang</button>

              </div>

            </div>
          </div>
        </div>
      </div>
  </div>
</form>
@endsection

<script>
    $('#tes').attr('hidden','hidden');
  $(document).ready(function(){
      $('#kamar').change(function(){
        var id_kamar = $(this).val();
        $('#data_kamar').removeAttr('hidden','hidden');

        $.post("{{ url('admin/order/get_kamar') }}",{
            'id_kamar':id_kamar,
        },function(data){
            var option = '<option value="">Pilih Kamar</option>';
            $(data).each(function(index,value){
                option += '<option value="'+value.id+'" >'+value.+'</option>';
            });
        });
      });
  });
</script>