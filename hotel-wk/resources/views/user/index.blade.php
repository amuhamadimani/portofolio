@extends('templates.stisla-user')

@section('title','Hotel-WK')
<style>
</style>
@section('header')
	<div class="section-header">
    <h1>Beranda</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active">
        <a href="{{ url('user') }}">Dashboard</a>
      </div>
    </div>
  </div>
@endsection
@section('content')

<div class="card">
	<div class="card-body">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('dist/img/hotel.jpg') }}" alt="First slide" style="width: 500px;">
      <div class="carousel-caption d-none d-md-block">
	    <h5>Hotel Ngendongin</h5>
	    <p>Jl.Siratul Mustaqim No.666 , Jakarta Barat Daya</p>
  		</div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('dist/img/kamar.jpg') }}" alt="Second slide" style="width: 500px;">
      <div class="carousel-caption d-none d-md-block">
	    <h5>Kamar VIP Bintang 5</h5>
	    <p>Menyediakan Berbagai Kelas Kamar yang nyaman dan family friendly</p>
	  </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('dist/img/kamar.jpg') }}" alt="Third slide">
      <div class="carousel-caption d-none d-md-block">
	    <h5>Ngendongin</h5>
	    <p>...</p>
	  </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
	</div>
</div>


@stop
