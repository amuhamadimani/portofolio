@extends('templates.stisla')

@section('title', 'Room Class')

@section('header')
	<div class="section-header">
		<h1>Room Class</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ url('admin/roomclass') }}">Room Class</a>
			</div>
			<div class="breadcrumb-item">Create</div>
		</div>
	</div>
@endsection
 
@section('content')
<form action="" method="post" enctype="multipart/form-data"></form>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<form action="{{ url('/admin/kamar/store') }}" method="post" enctype="multipart/form-data">
					@csrf
						<input type="hidden" name="id_roomclass" value="" class="form-control" required="" autocomplete="off">
						
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">No Kamar</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="number" name="no_kamar" class="form-control" required="" autocomplete="off">
				            </div>
						</div>
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Bed</label>
							<div class="col-sm-12 col-md-7">
				             	<select name="bed" class="form-control">
				             		<option value="Singel Bed">Singel Bed</option>
				             		<option value="Double Bed">Double Bed</option>
			                    </select>
				            </div>
						</div>
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Room Class</label>
							<div class="col-sm-12 col-md-7">
				             	<select name="roomclass" class="form-control">
				             		@foreach($room as $val)
										<option value="{{ getRoom($val->id_roomclass) }}">{{ getRoom($val->id_roomclass) }}</option>
				             		@endforeach
			                    </select>
				            </div>
						</div>

						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Harga</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="number" name="harga" class="form-control" required="" autocomplete="off">
				            </div>
						</div>
						<div class="form-group row mb-4">
							<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
							<div class="col-sm-12 col-md-7">
				             	<input type="file" name="gambar" class="form-control" required="" autocomplete="off">
				            </div>
						</div>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-sm-12 col-md-7">
				              <button class="btn btn-primary" type="submit">Tambah</button>
				              <a href="{{ url('admin/roomclass/create')}}" class="btn btn-warning ml-3">Kembali</a>
				            </div>
						</div>

					</form>	
				</div>
			</div>
		</div>
	</div>

@endsection