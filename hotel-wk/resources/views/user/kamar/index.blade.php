@extends('templates.stisla-user')

@section('title','Hotel-WK')
<style>
</style>
@section('header')
  <div class="section-header">
    <h1>List Kamar</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active">
        <a href="{{ url('user') }}">Dashboard</a>
      </div>
      <div class="breadcrumb-item">Detail Kamar</div>
    </div>
  </div>
@endsection

@section('content')

	<div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped" id="table-1">
                <thead>                                 
                  <tr>
                    <th class="text-center">
                      No
                    </th>
                    <th>No Kamar</th>
                    <th>Bed</th>
                    <th>Roomclass</th>
                    <th>Harga</th>
                    <th>Gambar</th>
                  </tr>
                </thead>
                <tbody class="text-center"> 
                @foreach($data as $val)                                
                  <tr>
                    <td>{{ $loop->index +1 }}</td>
                    <td>{{ $val->no_kamar }}</td>
                    <td>{{ $val->bed }}</td>
                    <td>{{ $val->roomclass }}</td>
                    <td>Rp. {{ number_format($val->harga,0,0,'.') }} / Malam</td>
                    <td><img src="{{ asset('assets/img/kamar/'.$val->gambar) }}" width="100" height="100" alt=""></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
