<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title')</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/summernote/summernote-bs4.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/jquery-selectric/selectric.css') }}">
  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{asset('/dist/modules/select2/dist/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{ asset('dist/modules/dropify/css/dropify.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/datatables/datatables.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('dist/modules/izitoast/css/iziToast.min.css')}}">

  <!-- ajax -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('dist/css/style.css') }}">
  {{-- <script src="{{ asset('js/sw.js') }}"></script> --}}
  <script src="{{ asset('dist/modules/jquery.min.js') }}"></script>
    <link href="https://vjs.zencdn.net/7.3.0/video-js.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:600|Source+Sans+Pro" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <style>
    .poppins
    {
      font-family: "Poppins", sans-serif;
    }

    .source-sans
    {
      font-family: 'Source Sans Pro', sans-serif;
    }

    .article-title
    {
      font-family: "Poppins", sans-serif;
      color: #44486B !important;
      font-weight: 700;
      line-height: 41px;
    }

    .article-content
    {
      font-family: "Source Sans Pro", sans-serif;
      color: #44486B !important;
      font-weight: 400;
      line-height: 24px;
      font-size: 18px;
    }

    .text-name{
      color: #44486B;
      font-weight: bold;
    }
  </style>
</head>

<body class="sidebar-mini">
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg bg-primary"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">

            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            {{-- <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li> --}}
          </ul>
          <div class="search-element">
            <!-- <input class="form-control" type="search" placeholder="Search" aria-label="Search"> -->
            <!-- <button class="btn" type="submit"><i class="fas fa-search"></i></button> -->
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-header">Notifications
                <div class="float-right">
                  <a href="#">Mark All As Read</a>
                </div>
              </div>
              <div class="dropdown-list-content">
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-2.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Kusnaedi</b> has moved task <b>Fix bug header</b> to <b>Done</b>
                    <div class="time">10 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-2.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Ujang Maman</b> has moved task <b>Fix bug footer</b> to <b>Progress</b>
                    <div class="time">12 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-3.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Agung Ardiansyah</b> has moved task <b>Fix bug sidebar</b> to <b>Done</b>
                    <div class="time">12 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-4.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Ardian Rahardiansyah</b> has moved task <b>Fix bug navbar</b> to <b>Done</b>
                    <div class="time">16 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-5.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Alfa Zulkarnain</b> has moved task <b>Add logo</b> to <b>Done</b>
                    <div class="time">Yesterday</div>
                  </div>
                </a>
              </div>
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </li>
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->name }}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title">Logged in 5 min ago</div>
              <a href="{{ url('trainer/profile') }}" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <a href="#" class="dropdown-item has-icon">
                <i class="fas fa-cog"></i> Settings
              </a>
              <div class="dropdown-divider"></div>
              <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="{{url('/admin')}}">HOTEL WK</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{url('admin')}}">H.W</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li><a class="nav-link" href="{{ url('/admin') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
            <li class="menu-header">List</li>
            <li><a class="nav-link" href="{{ route('kamar.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Data Kamar"><i class="fas fa-bed"></i><span>Data Kamar</span></a></li>
            <li><a class="nav-link" href="{{ route('order.index') }}"data-toggle="tooltip" data-placement="right" title data-original-title="Pesan Kamar"><i class="fas fa-play"></i> <span>Pesan Kamar</span></a></li>
            <li><a class="nav-link" href="{{ route('detailroom.index') }}"data-toggle="tooltip" data-placement="right" title data-original-title="Checkout"><i class="fas fa-power-off"></i> <span>Checkout</span></a></li>
            <!-- <li><a class="nav-link" href="{{ url('trainer/pdf') }}"><i class="fas fa-file"></i> <span>PDF</span></a></li> -->
          </ul>
        </aside>
      </div>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          @yield('header')

          <div class="section-body">
            @yield('content')
          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/"></a>
        </div>
        <div class="footer-right">
          v2.0.0
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
  
  <script src="{{ asset('dist/modules/popper.js') }}"></script>
  <script src="{{ asset('dist/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dist/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dist/modules/moment.min.js') }}"></script>
  <script src="{{ asset('dist/js/stisla.js') }}"></script>
  
  <!-- JS Libraies -->
  
  <script src="{{ asset('dist/modules/izitoast/js/iziToast.min.js')}}"></script>
  @include('templates.notification')
  

  <!-- Page Specific JS File -->
  <script src="{{ asset('dist/modules/summernote/summernote-bs4.js') }}"></script>
  <script src="{{ asset('dist/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
  <script src="{{ asset('dist/modules/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
  <script src="{{ asset('dist/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>

  <!-- Page Specific JS File -->
  <script src="{{ asset('dist/js/page/features-post-create.js') }}"></script>
  <script src="{{ asset('dist/modules/select2/dist/js/select2.min.js') }}"></script>
  <script src="{{ asset('dist/modules/dropify/js/dropify.min.js') }}"></script>
   <script src="{{ asset('dist/modules/datatables/datatables.min.js') }}"></script>
  <script src="{{ asset('dist/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('dist/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
  <script src="{{ asset('dist/modules/jquery-ui/jquery-ui.min.js') }}"></script>
  <script src="{{ asset('dist/js/page/modules-datatables.js') }}"></script>
  <script src="https://vjs.zencdn.net/7.3.0/video.js"></script>
  <!-- Template JS File -->
  <script src="{{ asset('dist/js/scripts.js') }}"></script>
  <script src="{{ asset('dist/js/custom.js') }}"></script>


  @include('component.swal_alert')
  @yield('script')
  <script>
    $(document).ready(function(){
      $('.datatable').DataTable();
    });
  </script>
    <script>
        $('.dropify').dropify({
          message: {
            'default' : '',
            'remove'  : 'Remove',
            'error' : 'Kesalahan Tipe File'
          }
        });
      $('#dabel').DataTable();

      $('#fasilitas'), function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')

        var modal = $(this)
        modal.find('.modal-body #id').val(id)
      }
      $(function(){
          // Set up the number formatting.
            $('#bayar').keyup(function(){
              var total = parseInt($('#total').val()); 
              var bayar = parseInt($('#bayar').val()); 
              var kembali = bayar - total;

              $("#kembalian").val(kembali);

              if (kembali < 0  || $("#kembalian").val() == '') {
                $("btnBayar").attr('disabled', 'disabled');
              }else{
                $("btnBayar").removeAttr('disabled');
              }
            });
      });
      $(document).ready(function() {
          $('#export').DataTable( {
              dom: 'Bfrtip',
              buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
              ]
          } );
      } );
      
    </script>
</body>
</html> 