<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_order');
            $table->integer('id_user');
            $table->string('nama');
            $table->string('email');
            $table->string('no_kamar');
            $table->date('checkin');
            $table->date('checkout');
            $table->string('durasi');
            $table->integer('total_harga');
            $table->integer('status_booking');
            $table->integer('status_checkout')->default(0);
            $table->enum('status_bayar',[
                'belum bayar',
                'dibayar'
            ])->default('belum bayar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
