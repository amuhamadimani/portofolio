<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomclassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roomclasses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_roomclass');
            $table->string('id_fasilitas');
            $table->string('roomclass');
            // $table->string('fasilitas');
            // $table->string('gambar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roomclasses');
    }
}
