<?php

namespace App\Http\Middleware;

use Closure;
use Auth;


class user
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->guard('web')->check()) {
            return redirect()->route('login');
        }

        if (isset(auth::user()->id_level)) {
            if (auth()->user()->id_level == 1) {
                return abort(404);
            }
            elseif (auth()->user()->id_level == 3) {
                return abort(404);
            }
        }
        return $next($request);
    }
}
