<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\roomclass;


class AjaxController extends Controller
{
    public function getKamar(Request $request){

        $key = $request->key;
        $room = roomclass::all();

        return response()->json([
            'html' => view('admin.kamar.add_kamar',['key'=>$key, 'room' => $room])->render(),
            'key' => $key,
            'room' => $room
        ]);
    }
}
