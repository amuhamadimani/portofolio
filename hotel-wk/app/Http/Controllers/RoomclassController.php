<?php

namespace App\Http\Controllers;

use App\Roomclass;
use App\detail_room;
use Validator;
use App\fasilitas;
use Illuminate\Http\Request;

class RoomclassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index()
    {
        // $data = detail_room::all();
        $data = roomclass::all();

        // dd($fasilitas);
        // $test = roomclass::find(18)->first();

        // $id_fasilitas = json_decode($test->id_fasilitas);
        // $fasilitasData = fasilitas::whereIn('id',$id_fasilitas)->get();

        
        return view('admin/roomclass/index', compact('data'));
    }  

    public function fasilitas()
    {
        return view('admin/roomclass/fasilitas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $a = roomclass::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'RM'.date('dmY').$id;
        
        $data = fasilitas::all();
        return view('admin/roomclass/create', compact('data', 'id_order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = Validator::make($request->all(),[
            'id_fasilitas[]' => 'required',
            'roomclass' => 'required',
            'gambar' => 'required'
        ]);

        $id_fasilitas = json_encode($request->id_fasilitas);

        $data = roomclass::create([
            'id_roomclass' => $request->id_roomclass,
            'roomclass' => $request->roomclass,
            'id_fasilitas' => $id_fasilitas
        ]);

        detail_room::create([
            'id_roomclass' => $request->id_roomclass,
        ]);

        return redirect('admin/roomclass')->with('success', 'Roomclass Berhasil Ditambahkan !');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roomclass  $roomclass
     * @return \Illuminate\Http\Response
     */
    public function show(Roomclass $roomclass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roomclass  $roomclass
     * @return \Illuminate\Http\Response
     */
    public function edit(Roomclass $roomclass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roomclass  $roomclass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roomclass $roomclass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roomclass  $roomclass
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roomclass $roomclass)
    {
        //
    }
}
