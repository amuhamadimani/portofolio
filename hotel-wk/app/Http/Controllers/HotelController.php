<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        return view('admin.data');
    }

    public function tamu()
    {
        return view('admin.tamu');
    }

    public function pemasukan()
    {
        return view('admin.pemasukan');
    }
}
