<?php

namespace App\Http\Controllers;

use App\order;
use App\kamar;
use DateTime;
use Validator;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $a = order::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        $kamar = kamar::where('status', 0)->get();

        return view('admin.order.index', compact('kamar', 'id_order'));
    }
    public function order_index()
    {
        $a = order::select('id')->latest()->value('id');
        $id = $a +1;
        $id_order = 'OR'.date('dmY').$id;

        $kamar = kamar::where('status', 0)->get();

        return view('user.order.index', compact('kamar', 'id_order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Validator::make($request->all(),[
            'id_order' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'no_kamar' => 'required',
            'checkin' => 'required',
            'checkout' => 'required',
            'booking' => 'required',
        ]);

        $fdate = $request->checkin;
        $edate = $request->checkout;
        $datetime1 = new DateTime($fdate);
        $datetime2 = new DateTime($edate);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');

        order::create([
            'id_order' => $request->id_order,
            'id_user' => $request->id_user,
            'nama' => $request->nama,
            'email' => $request->email,
            'no_kamar' => $request->no_kamar,
            'checkin' => $request->checkin,
            'checkout' => $request->checkout,
            'status_booking' => $request->booking,
            'status_booking' => $request->booking,
            'durasi' => $days,      
            'total_harga' => $days * getHarga($request->no_kamar),      
        ]);

        $room = kamar::where('id', $request->no_kamar)->update([
            'status' => 1,
        ]);

        return redirect('admin/order/transaksi/'.$request->id_order)->with('success', 'Kamar Anda Telah Di Proses');


    }
    public function user_store(Request $request)
    {
        $rules = Validator::make($request->all(),[
            'id_order' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'no_kamar' => 'required',
            'checkin' => 'required',
            'checkout' => 'required',
            'booking' => 'required',
        ]);

        $fdate = $request->checkin;
        $edate = $request->checkout;
        $datetime1 = new DateTime($fdate);
        $datetime2 = new DateTime($edate);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');

        order::create([
            'id_order' => $request->id_order,
            'id_user' => $request->id_user,
            'nama' => $request->nama,
            'email' => $request->email,
            'no_kamar' => $request->no_kamar,
            'checkin' => $request->checkin,
            'checkout' => $request->checkout,
            'status_booking' => $request->booking,
            'status_booking' => $request->booking,
            'durasi' => $days,      
            'total_harga' => $days * getHarga($request->no_kamar),      
        ]);

        $room = kamar::where('id', $request->no_kamar)->update([
            'status' => 1,
        ]);

        return redirect()->back()->with('success', 'Kamar Anda Telah Di Proses !');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kamar = kamar::findOrFail($id);

        return view('admin.order.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(order $order)
    {
        //
    }
}
