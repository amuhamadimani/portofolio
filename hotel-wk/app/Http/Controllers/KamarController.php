<?php

namespace App\Http\Controllers;

use Validator;
use App\kamar;
use App\roomclass;
use Illuminate\Http\Request;

class KamarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = kamar::all();
        return view('admin.kamar.index', compact('data'));
    }
    
    public function kamar_index()
    {
        $data = kamar::all();

        return view('user.kamar.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $room = roomclass::all();
        return view('admin.kamar.create', compact('room'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = Validator::make($request->all(),[
            'no_kamar' => 'required',
            'bed' => 'required',
            'roomclass' => 'required',
            'harga' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $do = new kamar($request->all());
        //$do->gambar = $request->file('gambar')->store('public/app/menu');

        $gambar = $request->file('gambar');

        $extention = $gambar->getClientOriginalName();
        $filename = pathinfo($extention, PATHINFO_FILENAME);
        $extention = $gambar->getClientOriginalExtension();
        $fileNameToStore = $filename . '_' . time() . '.' . $extention;
        $path = $gambar->move(public_path() . '/assets/img/kamar', $fileNameToStore);

        $do->gambar = $fileNameToStore;
        // dd($do);
        $do->save();


        return redirect('admin/kamar')->with('success', 'Kamar Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function show(kamar $kamar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room = roomclass::all();
        $edit = kamar::where('id', $id)->first();

        return view('admin.kamar.edit', compact('room', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = Validator::make($request->all(),[
            'no_kamar' => 'required',
            'bed' => 'required',
            'roomclass' => 'required',
            'harga' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $update = kamar::where('id', $id)->update([
            'no_kamar' => $request->no_kamar,
            'bed' => $request->bed,
            'roomclass' => $request->roomclass,
            'harga' => $request->harga,
        ]);


        return redirect('admin/kamar')->with('success', 'Kamar Berhasil Ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\kamar  $kamar
     * @return \Illuminate\Http\Response
     */
    public function destroy(kamar $kamar)
    {
        //
    }
}
