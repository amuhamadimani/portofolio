<?php

namespace App\Http\Controllers;

use App\detail_room;
use App\order;
use App\kamar;
use Illuminate\Http\Request;

class DetailRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = order::where('status_checkout', 0)->get();
        return view('admin.detail.index', compact('order'));
    }
    public function transaksi()
    {
        $order = order::where('status_bayar', 'belum bayar')->get();
        return view('admin.detail.transaksi', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        order::where('id_order', $id)->update([
            'status_checkout' => 1,
        ]);
        kamar::where('no_kamar', $request->no_kamar)->update([
            'status' => 0,
        ]);

        return redirect()->back()->with('success', 'Anda Telah Checkout');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\detail_room  $detail_room
     * @return \Illuminate\Http\Response
     */
    public function show(detail_room $detail_room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\detail_room  $detail_room
     * @return \Illuminate\Http\Response
     */
    public function edit(detail_room $detail_room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\detail_room  $detail_room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\detail_room  $detail_room
     * @return \Illuminate\Http\Response
     */
    public function destroy(detail_room $detail_room)
    {
        //
    }
}
