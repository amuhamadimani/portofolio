<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;
use App\kamar;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.report.index');
    }
    public function manager_index()
    {
        return view('manager.report.index');
    }

    public function transaksi()
    {
        $data = order::where('status_checkout', 1)->get();
        return view('manager.report.report_transaksi', compact('data'));
    }
    public function manager_transaksi()
    {
        $data = order::where('status_checkout', 1)->get();
        return view('manager.report.report_transaksi', compact('data'));
    }

    public function kamar()
    {
        $data = kamar::all();
        return view('admin.report.report_kamar', compact('data'));
    }
    public function manager_kamar()
    {
        $data = kamar::all();
        return view('order.report.report_kamar', compact('data'));
    }


    public function income()
    {
        $takhir = order::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            $sum = order::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->sum('total_harga');
            

            Session(['t1'=>$request->tanggal_awal]);
            Session(['t2'=>$request->tanggal_akhir]);
            

            return view('admin.report.report_transaksi_tanggal', ['data'=>$takhir, 't1'=>$request->tanggal_awal, 't2'=>$request->tanggal_akhir,'t3'=>$request->ontanggal, 'total'=>$sum]);
    }

    public function kamar_tanggal(Request $request)
    {
            $takhir = kamar::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            $sum = kamar::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            

            Session(['t1'=>$request->tanggal_awal]);
            Session(['t2'=>$request->tanggal_akhir]);
            

            return view('admin.report.report_kamar_tanggal', ['data'=>$takhir, 't1'=>$request->tanggal_awal, 't2'=>$request->tanggal_akhir,'t3'=>$request->ontanggal, 'total'=>$sum]);
    }

    public function manager_kamar_tanggal(Request $request)
    {
            $takhir = kamar::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            $sum = kamar::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            

            Session(['t1'=>$request->tanggal_awal]);
            Session(['t2'=>$request->tanggal_akhir]);
            

            return view('manager.report.report_kamar_tanggal', ['data'=>$takhir, 't1'=>$request->tanggal_awal, 't2'=>$request->tanggal_akhir,'t3'=>$request->ontanggal, 'total'=>$sum]);
    }

    public function transaksi_tanggal(Request $request)
    {
            $takhir = order::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            $sum = order::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            

            Session(['t1'=>$request->tanggal_awal]);
            Session(['t2'=>$request->tanggal_akhir]);
            

            return view('admin.report.report_transaksi_tanggal', ['data'=>$takhir, 't1'=>$request->tanggal_awal, 't2'=>$request->tanggal_akhir,'t3'=>$request->ontanggal, 'total'=>$sum]);
    }
    public function manager_transaksi_tanggal(Request $request)
    {
            $takhir = order::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            $sum = order::whereBetween('created_at',[$request->tanggal_awal, $request->tanggal_akhir])->get();
            

            Session(['t1'=>$request->tanggal_awal]);
            Session(['t2'=>$request->tanggal_akhir]);
            

            return view('manager.report.report_transaksi_tanggal', ['data'=>$takhir, 't1'=>$request->tanggal_awal, 't2'=>$request->tanggal_akhir,'t3'=>$request->ontanggal, 'total'=>$sum]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
