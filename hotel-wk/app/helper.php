<?php 

function getRoom($id){
	$menu = App\roomclass::select('roomclass')->where('id_roomclass', $id)->first();
	return $menu->roomclass;	
}
function getHarga($id){
	$menu = App\kamar::select('harga')->where('no_kamar', $id)->first();
	return $menu->harga;
}
function getFasilitas($id){
	$dataId = json_decode($id); 
	$menu = App\fasilitas::select('fasilitas')->whereIn('id',$dataId)->get();
	return $menu;	
}