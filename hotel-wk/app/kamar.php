<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kamar extends Model
{
    protected $table = 'kamars';
    protected $fillable = [
    	'no_kamar',
    	'bed',
    	'roomclass',
    	'harga',
    	'gambar',
    ];
}
