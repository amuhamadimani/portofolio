<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roomclass extends Model
{
    protected $table = 'roomclasses';

    protected $fillable = [
    	'id_roomclass',
    	'roomclass',
    	'id_fasilitas',
    ];

    public function fasilitas(){
    	return $this->hasMany('App\fasilitas','id','id_fasilitas');
    }
}
  