<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
    	'id_order',
    	'id_user',
    	'nama',
    	'email',
    	'no_kamar',
    	'id_kamar',
    	'checkin',
    	'checkout',
    	'status_booking',
    	'durasi',
    	'total_harga'
    ];
}
