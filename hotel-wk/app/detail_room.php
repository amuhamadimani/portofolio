<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_room extends Model
{
    protected $table = 'detail_rooms';
    protected $fillable = [
    	'id_roomclass'
    ];
}
