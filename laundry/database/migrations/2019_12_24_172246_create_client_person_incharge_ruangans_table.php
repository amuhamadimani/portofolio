<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientPersonInchargeRuangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_person_incharge_ruangan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_pic_id');
            $table->string('client_pic_name');
            $table->integer('client_pic_no_telp');
            $table->string('client_pic_ph1');
            $table->string('client_pic_ph2');
            $table->string('client_pic_email');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_person_incharge_ruangans');
    }
}
