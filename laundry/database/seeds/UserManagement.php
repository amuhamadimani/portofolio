<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserManagement extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->insert([
        //     'name' => 'Dadang',
        //     'email' => 'admin@system.com',
        //     'password' => bcrypt('password'),
        //     'level' => '1',
        // ]);
        // DB::table('users')->insert([
        //     'name' => 'Mamat',
        //     'email' => 'pickupboy@system.com',
        //     'password' => bcrypt('password'),
        //     'level' => '2',
        // ]);
        DB::table('users')->insert([
            'name' => 'Bambang',
            'email' => 'pickupclient@system.com',
            'password' => bcrypt('password'),
            'level' => '3',
        ]);
    }
}
