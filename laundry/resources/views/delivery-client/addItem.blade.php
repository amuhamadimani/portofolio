<div class="row" style="margin-top:10px" id="input-{{ $key }}">
    <div class="col-md-1"></div>
    <div class="col-md-2">
        <input type="text" class="form-control" name="item_code[{{ $key }}]" id="myorders">
    </div>
    <div class="col-md-4">
        <input type="text" class="form-control" name="item_name[{{ $key }}]" id="myorders">
    </div>
    <div class="col-md-2">
        <input type="number" class="form-control" name="qty[{{ $key }}]" id="myorders">
    </div>
    <div class="col-md-1">
        <button type="button" class="btn btn-danger" onclick="$(this).removeItem('#input-{{ $key }}')">x</button>
    </div>
</div>