@php
    $date = date('Y-m-d');    
    $jam = date('H:i:s');    
@endphp

@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Delivery Service - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ url('admin') }}">Dashboard</a>
			</div>
		</div>
	</div>
@endsection
    
@section('content')
<form action="{{ url('delivery-client/edit/'.$edit->id) }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Rumah Sakit</label>
                            <div class="col-md-7">
                                <input type="text" name="rumah_sakit" class="form-control" readonly value="{{ $edit->rumah_sakit }}" >
                            </div>
                        </div>    
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Lantai</label>
                            <div class="col-md-7">
                                <input type="text" name="lantai" class="form-control" readonly value="{{ $edit->lantai }}" >
                            </div>
                        </div>  
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Ruang Perawatan</label>
                            <div class="col-md-7">
                                <input type="text" name="ruang_perawatan" class="form-control" readonly value="{{ $edit->ruang_perawatan }}" >
                            </div>
                        </div>  
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">PIC</label>
                            <div class="col-md-7">
                                <input type="text" name="pic" class="form-control" readonly value="{{ $edit->pic }}" >
                            </div>
                        </div>   
                            
                    </div>
                
                    <div class="col-md-6">
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tanggal</label>
                            <div class="col-md-7">
                                <input type="text" name="tanggal" class="form-control" value="{{$edit->tanggal}}" readonly>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div>
                    <hr>
                    @foreach ($editItem as $item)
                        @php
                            $kode = json_decode($item->item_code);
                            $nama = json_decode($item->item_name);
                            $qty = json_decode($item->qty);
                            // dd($nama);
                        @endphp
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <h6 class="text-center">Item Code</h6>
                                    @foreach ($kode as $val)
                                        <input type="text" class="form-control" value="{{ $val }}" readonly name="item_code[]"><br>
                                    @endforeach
                                </div>
                                <div class="col-md-4">
                                    <h6 class="text-center">Item Name</h6>
                                    @foreach ($nama as $val)
                                        <input type="text" class="form-control" value="{{ $val }}" readonly name="nama_item[]"><br>
                                    @endforeach
                                </div>
                                <div class="col-md-2">
                                    <h6 class="text-center">Qty</h6>
                                    @foreach ($qty as $val)
                                        <input type="number" class="form-control" value="{{ $val }}" readonly name="qty[]"><br>
                                    @endforeach
                                </div>
                                {{-- <div class="col-md-1">
                                    <button type="button" class="btn btn-success" id="add-item"><i class="fa fa-plus"></i></button>
                                </div> --}}

                                {{-- <div class="col-md-1">
                                    <button type="button" class="btn btn-success" id="add-order"><i class="fa fa-plus"></i></button>
                                </div> --}}
                            </div>
                    @endforeach
                </div>

                    {{-- append ajax --}}
                    <div id="input-list"> </div>
                    <input type="hidden" id="key" value="0">
                    <br>

                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pickup Boy</label>
                            <div class="col-md-7">
                                <input type="text" name="pickup_boy" class="form-control" readonly value="{{ $edit->pickup_boy }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group row mb-4">
                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jam</label>
                            <div class="col-md-7">
                            <input type="text" name="jam" class="form-control" value="{{ $jam }}" readonly value="{{ $edit->jam }}">
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('pickup.client.create') }}">
                        <div class="col-md-1">
                            <button class="btn btn-success btn-lg">Approval</button>
                        </div>
                    </form>
                    
                </div>
                
        </div>
        </div>
    </div>
</form>
@endsection

@section('script')
  
    <script>
        $(document).ready(function(){

            var key_input = $("#key").val();

            $("#add-item").on("click", function(e){ 

                e.preventDefault();

                key_input++;

                var url = '/ajax/get-item-client'

                var data = {
                    key:key_input
                }
                
                $.ajax({
                    url:url,
                    data:data,
                    type:"POST",
                    success:function(result){
                        $("#input-list").append(result.html);
                        $("#key").val(result.key);
                    }
                })
            
            });
        });

        $.fn.removeItem = function(id){
            $(id).remove();
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
    </script>

@endsection
