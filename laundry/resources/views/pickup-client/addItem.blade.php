<div class="row" style="margin-top:10px" id="input-{{ $key }}">
    <div class="col-md-1"></div>
    <div class="col-md-2">
        <select name="item_code[{{ $key }}]" id="get-item" class="form-control">
            <option value="">Pilih Item</option>
            @foreach ($kontrak as $val)
                @php
                    $kode = json_decode($val->kode_item);
                @endphp
                @foreach ($kode as $val)
                    <option value="">{{ $val }}</option>
                @endforeach
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        <select name="item_name[{{ $key }}]" id="get-item" class="form-control">
            <option value="">Pilih Item</option>
            @foreach ($kontrak as $val)
                @php
                    $nama = json_decode($val->nama_item);
                @endphp
                @foreach ($nama as $val)
                    <option value="">{{ $val }}</option>
                @endforeach
            @endforeach
        </select>
    </div>
    <div class="col-md-2">
        <input type="number" class="form-control" name="qty[{{ $key }}]" id="myorders">
    </div>
    <div class="col-md-1">
        <button type="button" class="btn btn-danger" onclick="$(this).removeItem('#input-{{ $key }}')">x</button>
    </div>
</div>