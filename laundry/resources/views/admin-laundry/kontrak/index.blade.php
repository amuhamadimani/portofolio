@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Admin - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ route('admin.laundry.index') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('admin.laundry.pickup.boy.index') }}">Pickup Boy</a>
			</div>
			<div class="breadcrumb-item active">
				<a>Create</a>
			</div>
		</div>
	</div>
@endsection
    
@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('/admin') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('admin.laundry.pickup.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup & Delivery Boy"><i class="fas fa-user"></i><span>Pickup & Delivery Boy</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Client"><i class="fas fa-user"></i><span>Maintenance Client</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Kontrak"><i class="fas fa-file"></i><span>Maintenance Kontrak</span></a></li>
    <li><a class="nav-link" href="{{ route('users') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Management User"><i class="fas fa-users"></i><span>Management User</span></a></li>
   </ul>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">  
              <table class="table table-bordered table-striped table-hover" id="dabel">
                <a href="{{ route('admin.laundry.kontrak.create') }}" class="a btn btn-info btn-lg float-right"><i class="fa fa-plus"></i></a>
                <br>
                <h4 class="card-title">Maintenance Kontrak</h4>

                  <thead class="text-center">
                      <tr>
                          <th>No</th>
                          <th>No Kontrak</th>
                          <th>Perusahaan Client</th>
                          <th>Dari Tanggal</th>
                          <th>Sampai Tanggal</th>
                          <th>Jenis</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      
                      @foreach ($data as $val)
                          {{-- @php
                              $item = json_decode($val->nama_item);
                          @endphp --}}
                    {{-- @foreach ($item as $file)
                          {{$file}},
                          @endforeach --}}
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $val->no_kontrak }}</td>
                            <td>{{ $val->client }}</td>
                            <td>{{ $val->periode_kontrak_mulai }}</td>
                            <td>{{ $val->periode_kontrak_akhir }}</td>
                            <td>{{ $val->jenis_kegiatan }}</td>
                            <td>
                              <a href="{{ url('admin-laundry/kontrak/edit/'.$val->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                              {{-- <a href="{{ url('admin-laundry/kontrak/delete/'.$val->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a> --}}
                            </td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>  
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection