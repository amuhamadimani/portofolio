@php
    $date = date('Y-m-d');    
    $jam = date('H:i:s');    
@endphp

@extends('templates.stisla')

@section('title', 'Laundry')

@section('header')
	<div class="section-header">
		<h1>Pickup Service - Dashboard</h1>
		<div class="section-header-breadcrumb">
			<div class="breadcrumb-item active">
				<a href="{{ route('admin.laundry.index') }}">Dashboard</a>
			</div>
			<div class="breadcrumb-item active">
				<a href="{{ route('admin.laundry.pickup.boy.index') }}">Pickup Boy</a>
			</div>
			<div class="breadcrumb-item active">
				<a>Edit</a>
			</div>
		</div>
	</div>
@endsection
    
@section('sidebar-menu')
  <ul class="sidebar-menu">
    <li class="menu-header">Dashboard</li>
    <li><a class="nav-link" href="{{ url('/admin') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Dashboard"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    <li class="menu-header">List</li>
    <li><a class="nav-link" href="{{ route('admin.laundry.pickup.boy.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Pickup & Delivery Boy"><i class="fas fa-user"></i><span>Pickup & Delivery Boy</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Client"><i class="fas fa-user"></i><span>Maintenance Client</span></a></li>
    <li><a class="nav-link" href="{{ route('admin.laundry.client.index') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Maintenance Kontrak"><i class="fas fa-file"></i><span>Maintenance Kontrak</span></a></li>
    <li><a class="nav-link" href="{{ route('users') }}" data-toggle="tooltip" data-placement="right" title data-original-title="Management User"><i class="fas fa-users"></i><span>Management User</span></a></li>
   </ul>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
        <form action="{{ url('admin-laundry/pickup-boy/update/'.$edit->id) }}">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                        <div class="col-md-7">
                            <input type="text" name="nama" class="form-control" required="" value="{{ $edit->nama }}" >
                        </div>
                    </div>    
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">HP</label>
                        <div class="col-md-7">
                            <input type="text" name="hp" class="form-control" required="" value="{{ $edit->hp }}" >
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Sex</label>
                        <div class="col-md-7">
                            <input type="text" name="sex" class="form-control" required="" value="{{ $edit->sex }}" >
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Alamat</label>
                        <div class="col-md-7">
                        <textarea name="alamat" class="form-control" id="" cols="30" rows="10">{{ $edit->alamat }}</textarea>
                        </div>
                    </div>   
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Pekerjaan</label>
                        <div class="col-md-7">
                            <input type="text" name="pekerjaan" class="form-control" required="" value="{{ $edit->pekerjaan }}" >
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Agama</label>
                        <div class="col-md-7">
                            <input type="agama" name="agama" class="form-control" required="" value="{{ $edit->agama }}" >
                        </div>
                    </div>  
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tgl Lahir</label>
                        <div class="col-md-7">
                            <input type="date" name="tgl_lahir" class="form-control" required="" value="{{ $edit->tgl_lahir }}" >
                        </div>
                    </div>  
                        
                </div>
            
                <div class="col-md-6">
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Suku</label>
                        <div class="col-md-7">
                            <input type="text" name="suku" class="form-control" required="" value="{{ $edit->suku }}" >
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Provinsi</label>
                        <div class="col-md-7">
                            <input type="text" name="provinsi" class="form-control" required="" value="{{ $edit->provinsi }}" >
                        </div>
                    </div>
                    
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kabupaten</label>
                        <div class="col-md-7">
                            <input type="text" name="kabupaten" class="form-control" required="" value="{{ $edit->kabupaten }}" >
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kecamatan</label>
                        <div class="col-md-7">
                            <input type="text" name="kecamatan" class="form-control" required="" value="{{ $edit->kecamatan }}" >
                        </div>
                    </div>
                        <div class="card-footer text-right">
                            <a href="{{ route('admin.laundry.pickup.boy') }}" class="btn btn-danger btn-lg">Batal</a>
                            <button class="btn btn-primary btn-lg">Simpan</button>
                        </div>
                    </form>
                    
                </div>
      </div>
    </div>
</div>
@endsection

