@if(session('success'))
	<script>
		$(document).ready(function(){
			iziToast.success({
				title: 'Data Success',
				message: "{{ session('success') }}",
				position: 'topRight'
			});
		});
	</script>
	
@elseif(session('error'))
	<script>
		$(document).ready(function(){
		    iziToast.error({
			    title: 'Deleting Data Success!',
			    message: '{{ session('error') }}',
			    position: 'topRight'
		    });
		});
	</script>
@endif