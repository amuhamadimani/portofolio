<?php

namespace App\Http\Controllers\PickupDeliveryClient;

use App\Model\PickupClient;
use App\Model\Kontrak;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PickupClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PickupClient::all();
        
        return view('pickup-client.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $kontrak = Kontrak::all();

        // dd($request->isMethod('POST'));
        if ($request->isMethod('POST')) 
        {
            // dd($request->isMethod('POST'));
            $request->validate([
                'rumah_sakit' => 'required',
                'lantai' => 'required',
                'ruang_perawatan' => 'required',
                'pic' => 'required',
                'tanggal' => 'required',
                'item_code' => 'required',
                'item_name' => 'required',
                'qty' => 'required',
                'pickup_boy' => 'required',
                'jam' => 'required',
            ]);
            
            PickupClient::create([
                'rumah_sakit' => $request->rumah_sakit,
                'lantai' => $request->lantai,
                'ruang_perawatan' => $request->ruang_perawatan,
                'pic' => $request->pic,
                'tanggal' => $request->tanggal,
                'item_code' => json_encode($request->item_code),
                'item_name' => json_encode($request->item_name),
                'qty' => json_encode($request->qty),
                'pickup_boy' => $request->pickup_boy,
                'jam' => $request->jam,
            ]);

            return redirect('pickup-client')->with('success', 'Data Berhasil Ditambahkan');
        }

        return view('pickup-client.create', compact('kontrak'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createItem(Request $request)
    {
        
        return redirect()->back()->with('success', 'Item Telah Ditambahkan !');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\PickupClient  $pickupClient
     * @return \Illuminate\Http\Response
     */
    public function show(PickupClient $pickupClient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\PickupClient  $pickupClient
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $edit = PickupClient::where('id', $id)->first();
        $editItem = PickupClient::select('item_code', 'item_name', 'qty')->where('id', $id)->get();
        
        if ($request->isMethod('POST')) 
        {
            PickupClient::where('id', $id)->update([
                'status' => 'c',
            ]);

            return redirect('pickup-client')->with('success', 'Data Berhasil Diapprove');
        }

        return view('pickup-client.edit', compact('edit', 'editItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\PickupClient  $pickupClient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PickupClient $pickupClient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\PickupClient  $pickupClient
     * @return \Illuminate\Http\Response
     */
    public function destroy(PickupClient $pickupClient)
    {
        //
    }
}
