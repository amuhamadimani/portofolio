<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Model\Client;
use App\Model\Kontrak;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Client::all();
        return view('admin-laundry.client.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kontrak = Kontrak::all();
        return view('admin-laundry.client.create', compact('kontrak'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'no_client'  => 'required',
        //     'no_kontrak'  => 'required',
        //     'tanggal'  => 'required',
        //     'nama'  => 'required',
        //     'alamat'  => 'required',
        //     'telepon'  => 'required',
        //     'pic'  => 'required',
        //     'provinsi'  => 'required',
        //     'kabupaten'  => 'required',
        //     'kecamatan'  => 'required',
        //     'kode_pos'  => 'required',
        // ]);
        // dd($request->no_kontrak);
        
        Client::create([
            'no_client' => $request->no_client,
            'no_kontrak' => $request->no_kontrak,
            'tanggal' => $request->tanggal,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'telepon' => $request->telepon,
            'pic' => $request->pic,
            'provinsi' => $request->provinsi,
            'kabupaten' => $request->kabupaten,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
        ]);

        return redirect('admin-laundry/client')->with('success', 'Data Berhasi Ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Client::where('id', $id)->first();
        return view('admin-laundry.client.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama',
            'hp',
            'sex',
            'alamat',
            'pekerjaan',
            'agama',
            'tgl_lahir',
            'suku',
            'provinsi',
            'kabupaten',
            'kecamatan',
        ]);

        Client::where('id', $id)->update([
            'nama' => $request->nama,
            'hp' => $request->hp,
            'sex' => $request->sex,
            'pekerjaan' => $request->pekerjaan,
            'agama' => $request->agama,
            'alamat' => $request->alamat,
            'tgl_lahir' => $request->tgl_lahir,
            'suku' => $request->suku,
            'provinsi' => $request->provinsi,
            'kabupaten' => $request->kabupaten,
            'kecamatan' => $request->provinsi,
        ]);

        return redirect('admin-laundry/client')->with('success', 'Data Berhasi Diupdate !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
