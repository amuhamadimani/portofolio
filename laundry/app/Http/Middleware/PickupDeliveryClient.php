<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PickupDeliveryClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->guard('web')->check()) {
            return redirect()->route('login');
        }

        if (isset(auth::user()->level)) {
            if (auth()->user()->level == 1) {
                return redirect()->back();
            }
            elseif (auth()->user()->level == 2) {
                return redirect()->back();
            }
        }
        return $next($request);
    }
}
