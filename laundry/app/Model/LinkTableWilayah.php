<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LinkTableWilayah extends Model
{
    protected $table = 'link_table_wilayah';

    protected $fillable = [
        'provinsi_id',
        'kecamatan_id',
        'kabupaten_id',
        'kelurahan_id',
    ];
}
