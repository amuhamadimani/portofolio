<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    protected $table = 'ruangan';

    protected $fillable = [
        'ruangan_name',
        'ruangan_jmlh_kamar',
    ];

}
