<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'kecamatan';

    protected $fillable = [
        'kabupaten_id',
        'provinsi_id',
        'kecamatan_name',
    ];
}
