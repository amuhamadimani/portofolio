<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientAccounting extends Model
{
    protected $table = 'client_accounting';

    protected $fillable = [
        'client_acc_id',
        'client_acc_name',
        'client_acc_no_telp',
        'client_acc_ph1',
        'client_acc_ph2',
        'client_acc_email',
    ];
}
